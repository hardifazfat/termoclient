<?php

include("config.php");
$method = $_SERVER['REQUEST_METHOD'];

$result = array();

if($method == 'POST'){
    if(isset($_POST['action_id'])){
        $action_id = $_POST['action_id'];

        if($action_id == constant("ACTION_ID_LOGIN"))
        {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $statement = "SELECT role FROM user WHERE username = '$username' AND password = '$password'";
            $que = mysqli_query($conn, $statement);

            $role = mysqli_fetch_assoc($que)['role'];

            if(mysqli_num_rows($que) > 0){
                session_start();
                $_SESSION['username'] = $username;
                $_SESSION['role'] = $role;

                header("location: ../front/");
            }else{
                header("location: ../front/login.html");
                echo "<h2>Username atau Password Salah!</h2>";
            }
        }
        else if($action_id == constant("ACTION_ID_POST_NEW_STEAM")){
            $label = $_POST['label'];
            $address = $_POST['address'];

            $statement = "INSERT INTO steam VALUES(NULL,'$label','$address')";
            $que = mysqli_query($conn, "$statement");

            $result['status'] = [
                "code" => 200,
                "description" => 'insert new steam Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_POST_TASK_FINISH")){
            $task_id = $_POST['task_id'];
            $date = date("YmdHis");

            $statement = "UPDATE history SET remark = 'FINISH', finish_timestamp = '$date' WHERE task_id = '$task_id'";
            $que = mysqli_query($conn, "$statement");

            $statement = "DELETE FROM active_task WHERE task_id = '$task_id'";
            $que = mysqli_query($conn, "$statement");

            $statement = "DELETE FROM recent WHERE task_id = '$task_id'";
            $que = mysqli_query($conn, "$statement");

            $result['status'] = [
                "code" => 200,
                "description" => 'Stop task Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_POST_STOP_TASK")){
            $task_id = $_POST['task_id'];
            $date = date("YmdHis");

            $statement = "UPDATE history SET remark = 'ABORTED', finish_timestamp = '$date' WHERE task_id = '$task_id'";
            $que = mysqli_query($conn, "$statement");

            $statement = "INSERT INTO force_stop VALUES(NULL,'$task_id')";
            $que = mysqli_query($conn, "$statement");

            $statement = "DELETE FROM active_task WHERE task_id = '$task_id'";
            $que = mysqli_query($conn, "$statement");

            $result['status'] = [
                "code" => 200,
                "description" => 'Stop task Sukses !'
            ];
        }
        
        else if($action_id == constant("ACTION_ID_POST_ADDITIONAL_TIME")){
            $task_id = $_POST['task_id'];
            $add_time = $_POST['time_value'];

            $recent_additional = mysqli_fetch_assoc(mysqli_query($conn, "SELECT additional FROM task WHERE id = '$task_id'"))['additional'];
            $additional_val = $recent_additional . ',,,' . $add_time;

            $statement = "UPDATE task SET additional = '$additional_val' WHERE id = '$task_id'";
            $que = mysqli_query($conn, "$statement");

            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses menambah waktu additional'
            ];
        }
        else if($action_id == constant("ACTION_ID_POST_NEW_RESEP")){
            $resep_name = $_POST['resep_nama'];
            $resep_durasi = $_POST['resep_durasi'];
            $resep_id = $_POST['resep_id'];
            $troli_satu = $_POST['troli_satu'];
            $troli_dua = $_POST['troli_dua'];
            $troli_tiga = $_POST['troli_tiga'];
            $std_time_max = $_POST['std_time_max'];
            $std_time_min = $_POST['std_time_min'];
            $qpb = $_POST['qpb'];
            
            $statement = "";

            if($resep_id == "none"){
                $statement = "INSERT INTO resep VALUES(NULL,'$resep_name','$resep_durasi','$troli_satu','$troli_dua','$troli_tiga','$std_time_min','$std_time_max', '$qpb')";
                $result['status'] = [
                    "code" => 200,
                    "description" => 'Create resep Sukses !'
                ];
            }
            else{
                if($resep_name == ''){
                    $statement = "DELETE FROM resep WHERE id = '$resep_id'";
                    $result['status'] = [
                        "code" => 200,
                        "description" => 'Delete resep Sukses !'
                    ];
                }
                else{
                    $statement = "UPDATE resep SET nama = '$resep_name', durasi = '$resep_durasi', troli_satu = '$troli_satu', troli_dua = '$troli_dua', troli_tiga = '$troli_tiga', std_cook_min = '$std_time_min', std_cook_max = '$std_time_max', per_batch = '$qpb' WHERE id = '$resep_id'";
                    $result['status'] = [
                        "code" => 200,
                        "description" => 'Update resep Sukses !'
                    ];
                }
                
            }

            $que = mysqli_query($conn, "$statement");
            
        }
        else if($action_id == constant("ACTION_ID_POST_NEW_USER")){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $role = $_POST['role'];
            
            $statement = "INSERT INTO user VALUES(NULL,'$username','$password','$role')";
            $que = mysqli_query($conn, "$statement");
            
            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_DELETE_STOP_TASK")){
            $nama = $_POST['task_id'];

            $statement = "DELETE FROM force_stop WHERE task_id = '$nama'";
            $que = mysqli_query($conn, "$statement");
            
            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_POST_NEW_COLOR")){
            $nama = $_POST['nama'];
            
            $statement = "INSERT INTO color VALUES(NULL,'$nama')";
            $que = mysqli_query($conn, "$statement");
            
            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_UPDATE_RECENT"))
        {
            $pv = $_POST['pv'];
            $sv = $_POST['sv'];
            $task_id = $_POST["task_id"];
            $is_latch = $_POST["is_latch"];
            $is_latch_2 = $_POST["is_latch_2"];
            $ts = date("YmdHis");

            $recent_cooking_time = mysqli_fetch_assoc(mysqli_query($conn, "SELECT cooking_timestamp FROM history WHERE task_id = '$task_id'"))['cooking_timestamp'];
            
            if($pv >= 100){
                if($recent_cooking_time == '0'){
                    $statement = "UPDATE history SET cooking_timestamp='$ts' WHERE task_id='$task_id'";
                    $que = mysqli_query($conn, "$statement");
                }
                
            }

            $statement = "UPDATE recent SET pv='$pv', sv='$sv', ts='$ts', is_latch = '$is_latch', is_latch_2 = '$is_latch_2' WHERE task_id='$task_id'";
            $que = mysqli_query($conn, "$statement");
            
            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_DELETE_TASK"))
        {
            $id = $_POST['id'];
            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
            $que = mysqli_query($conn, "DELETE FROM task WHERE id=" . $id);      
        }
        else if($action_id == constant("ACTION_ID_UPDATE_HEATING_STD")){
            $durasi = $_POST['durasi'];

            $statement = "UPDATE setting_variables SET heating_std = '$durasi' WHERE id = '1'";
            $que = mysqli_query($conn, "$statement");

            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
        }
        else if($action_id == constant("ACTION_ID_POST_NEW_TASK"))
        {
            $mesin_id = $_POST['mesin_id'];
            $resep_id = $_POST['resep_id'];
            $resep_id_val = mysqli_fetch_assoc(mysqli_query($conn, "SELECT id FROM resep WHERE nama = '$resep_id'"))['id'];
            $items = $_POST['items'];
            $colors = $_POST['colors'];
            $lots = $_POST['lots'];
            $pic = $_POST['pic'];
            $additional = $_POST['additional'];
            $troli = $_POST['troli'];
            $ket = $_POST['ket'];
            $id = date("YmdHis");

            if($troli == "troli_satu"){
                $troli = "Troli Flip";
            }
            else if($troli == "troli_dua"){
                $troli = "Troli Biasa";
            }
            else if($troli == "troli_tiga"){
                $troli = "Troli Longgar";
            }
            else if($troli == "troli_culikan"){
                $troli = "Culikan";
            }
            else if($troli == "troli_sisa"){
                $troli = "Sisa Batch";
            }
            else if($troli == "troli_trial"){
                $troli = "Trial Resep";
            }


            $statement = "INSERT INTO task VALUES(NULL,'$mesin_id','$resep_id_val','$items','$colors','$lots','$pic','$additional','$id','$troli')";
            $que = mysqli_query($conn, "$statement");

            $statement = "INSERT INTO culikan_result VALUES(NULL,'$resep_id_val','$id','$additional')";
            $que = mysqli_query($conn, "$statement");

            $statement = "INSERT INTO history VALUES(NULL,'$id','$id',0,0,0)";
            $que = mysqli_query($conn, "$statement");

            $statement = "INSERT INTO active_task VALUES(NULL,'$id')";
            $que = mysqli_query($conn, "$statement");

            $statement = "INSERT INTO recent VALUES(NULL,'$id', '0', '0', '0', '0', '0')";
            $que = mysqli_query($conn, "$statement");

            $result['status'] = [
                "code" => 200,
                "description" => 'Sukses !'
            ];
        }
       
    }
    else{
        $result['status'] = [
            "code" => 400,
            "description" => 'Data tidak lengkap'
        ];
    }
}
else if($method == 'GET'){
    if(isset($_GET['action_id'])){
        $action_id = $_GET['action_id'];

        if($action_id == constant("ACTION_ID_GET_ACTIVE_TASK"))
        {
            $que_active_task = mysqli_query($conn, "SELECT * FROM active_task");
            $que_task = mysqli_query($conn, "SELECT * FROM task");
            $que_history = mysqli_query($conn, "SELECT * FROM history");
            $que_steam = mysqli_query($conn, "SELECT * FROM steam");
            $que_resep = mysqli_query($conn, "SELECT * FROM resep");
            $que_user = mysqli_query($conn, "SELECT * FROM user");
            $que_color = mysqli_query($conn, "SELECT * FROM color");
            $que_recent = mysqli_query($conn, "SELECT * FROM recent");
            $que_stop = mysqli_query($conn, "SELECT * FROM force_stop");
            $que_heat_std = mysqli_query($conn, "SELECT heating_std FROM setting_variables WHERE id = '1'");
            $que_culikan = mysqli_query($conn, "SELECT * FROM culikan_result");

            $result['status'] = [
            "code" => 200,
            "description" => 'Request GET'
            ];

            $result['value_active_task'] = $que_active_task->fetch_all(MYSQLI_ASSOC);
            $result['value_task'] = $que_task->fetch_all(MYSQLI_ASSOC);
            $result['value_history'] = $que_history->fetch_all(MYSQLI_ASSOC);
            $result['value_steam'] = $que_steam->fetch_all(MYSQLI_ASSOC);
            $result['value_resep'] = $que_resep->fetch_all(MYSQLI_ASSOC);
            $result['value_user'] = $que_user->fetch_all(MYSQLI_ASSOC);
            $result['value_color'] = $que_color->fetch_all(MYSQLI_ASSOC);
            $result['value_recent'] = $que_recent->fetch_all(MYSQLI_ASSOC);
            $result['value_force'] = $que_stop->fetch_all(MYSQLI_ASSOC);
            $result['value_heating_std'] = $que_heat_std->fetch_all(MYSQLI_ASSOC);
            $result['value_culikan'] = $que_culikan->fetch_all(MYSQLI_ASSOC);
        }    
    }
    else{
        $result['status'] = [
            "code" => 400,
            "description" => 'Data tidak lengkap'
        ];
    }
}

else{
    
    $result['status'] = [
        "code" => 201,
        "description" => 'Request Not Valid'
    ];
}

echo json_encode($result);
?>
