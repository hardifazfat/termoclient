<?php

header("Content-type:application/json");
date_default_timezone_set('Asia/Jakarta');
define("ACTION_ID_DELETE_TASK", 1);
define("ACTION_ID_UPDATE_RECENT", 2);
define("ACTION_ID_POST_NEW_TASK", 3);
define("ACTION_ID_GET_ACTIVE_TASK", 4);
define("ACTION_ID_LOGIN", 5);
define("ACTION_ID_POST_NEW_RESEP", 6);
define("ACTION_ID_POST_NEW_USER", 7);
define("ACTION_ID_POST_NEW_COLOR", 8);
define("ACTION_ID_POST_STOP_TASK", 9);
define("ACTION_ID_POST_ADDITIONAL_TIME", 10);
define("ACTION_ID_POST_TASK_FINISH", 11);
define("ACTION_ID_DELETE_STOP_TASK", 12);
define("ACTION_ID_UPDATE_HEATING_STD", 13);
define("ACTION_ID_POST_NEW_STEAM", 14);

//commentio

define("ROLE_USER_SUPER_ADMIN", "sa");

$conn = mysqli_connect("localhost","root","","termocont");