<?php

header("Content-type:application/json");

$conn = mysqli_connect("localhost","root","","termocon");
$method = $_SERVER['REQUEST_METHOD'];

$result = array();

if($method == 'GET'){
    $que = mysqli_query($conn, "SELECT * FROM mesin");
    $result['status'] = [
        "code" => 200,
        "description" => 'Request GET'
    ];
    $result['value'] = $que->fetch_all(MYSQLI_ASSOC);
}
else{
    $result['status'] = [
        "code" => 201,
        "description" => 'Request Not Valid'
    ];
}
//$result = mysqli_query($conn, "INSERT INTO mesin values ('', 'MESIN2')");
//var_dump(mysqli_fetch_row($result));

echo json_encode($result);
