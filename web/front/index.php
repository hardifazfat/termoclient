<?php
    session_start();
    if (!isset($_SESSION['username'])){
        header("Location: login.html");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Steambox Monitoring</title>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <link rel="stylesheet" href="./ext-modules/node_modules/animate.css/animate.css?n=1" />
        <link rel="stylesheet" href="bootstrap-5.1.3-dist/css/bootstrap.css?n=1" />
        <link href="css/style.css?n=1" rel="stylesheet" />
    </head>
    <body>
        <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
            <div class="d-flex flex-row col-md-8 col-lg-8 col-xl-8 col-xxl-6">
                <div class="navbar-nav w-100">
                    <div class="navbar-brand w-100"><div style="background-size: contain; background-repeat: no-repeat; background-image: url('./img/logo_wide.png'); height: 32px"> </div></div>
                </div>
            </div>
            <div class="d-flex flex-row w-50 justify-content-end">
                <div class="navbar-nav">
                    <div class="nav-item text-nowrap text-white px-3 nav-link">
                        Hi,
                        <?php if (isset($_SESSION['username'])){
                            echo $_SESSION['username'];}?>
                    </div>
                </div>
                <div class="navbar-nav">
                    <div class="nav-item text-nowrap">
                        <a class="nav-link px-3 logout me-2" href="../backend/session-logout.php">Logout</a>
                    </div>
                </div>
            </div>
        </header>
        <!-- Modal Three Row-->
        <div class="modal fade" id="modal-pr-ptr">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="fs-3 text-center w-100 fw-light text-secondary mt-3">PERHATIAN !</div>
                    <div class="modal-body d-block">
                        <input class="d-none" type="input" id="modal-same-batch-for" />
                        <div class="fs-6 text-center w-100 fw-lighter text-secondary mt-4" id="pr-ptr">aNDA AKAN</div>
                        <div class="d-flex justify-content-center flex-column col-12 mt-5">
                        <div class="col-6 my-1">
                            <button id="pr-ptr-btn-1" type="button" class="btn btn-secondary col-12">OK</button>
                        </div>
                        <div class="col-6 my-1">
                            <button id="pr-ptr-btn-2" type="button" class="btn btn-secondary col-12">OK</button>
                        </div>
                        <div class="col-6 my-1">
                            <button id="pr-ptr-btn-3" type="button" class="btn btn-secondary col-12">OK</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Prompt YES-->
        <div class="modal fade" id="modal-pr-py">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="fs-3 text-center w-100 fw-light text-secondary mt-3">PERHATIAN !</div>
                    <div class="modal-body d-block">
                        <input class="d-none" type="input" id="modal-same-batch-for" />
                        <div class="fs-6 text-center w-100 fw-lighter text-secondary mt-4" id="pr-py-msg">aNDA AKAN</div>
                        <div class="d-flex justify-content-center flex-row col-12 mt-5">
                            <div class="col-6">
                                <button id="pr-py-ok" type="button" class="btn btn-dark col-12">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Prompt YES/NO-->
        <div class="modal fade" id="modal-pr-pyn">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="fs-3 text-center w-100 fw-light text-secondary mt-3">PERHATIAN !</div>
                    <div class="modal-body d-block">
                        <input class="d-none" type="input" id="modal-same-batch-for" />
                        <div class="fs-6 text-center w-100 fw-lighter text-secondary mt-4" id="pr-pyn-msg">aNDA AKAN</div>
                        <div class="d-flex justify-content-center flex-row col-12 mt-5">
                            <div class="col-6 me-1">
                                <button id="pr-pyn-cancel" type="button" class="btn btn-secondary col-12">Tidak</button>
                            </div>
                            <div class="col-6 ms-1">
                                <button id="pr-pyn-ok" type="button" class="btn btn-dark col-12">Ya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="main-container">
            <div class="row">
                <nav id="sidebarMenu" class="col-md-2 col-lg-2 col-xl-2 col-xxl-1 d-md-block bg-light sidebar collapse">
                    <div class="position-sticky pt-4">
                        <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" id="a-home" onclick="home()">
                                <div class="d-flex flex-row align-items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"
                                    />
                                    </svg>
                                    <div class="mx-2">Home</div>
                                </div>
                            </a>
                            
                        </li>
                        <li class="nav-item" id="li-warna">
                            <a class="nav-link" href="#" onclick="warna()"
                                ><div class="d-flex flex-row align-items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-palette-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M12.433 10.07C14.133 10.585 16 11.15 16 8a8 8 0 1 0-8 8c1.996 0 1.826-1.504 1.649-3.08-.124-1.101-.252-2.237.351-2.92.465-.527 1.42-.237 2.433.07zM8 5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm4.5 3a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zM5 6.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm.5 6.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"
                                    />
                                    </svg>
                                    <div class="mx-2">Warna</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" onclick="daftarAkun()">
                                <div class="d-flex flex-row align-items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
                                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                    <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                    </svg>
                                    <div class="mx-2">Daftar Akun</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" onclick="resep()">
                                <div class="d-flex flex-row align-items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-receipt" viewBox="0 0 16 16">
                                    <path
                                        d="M1.92.506a.5.5 0 0 1 .434.14L3 1.293l.646-.647a.5.5 0 0 1 .708 0L5 1.293l.646-.647a.5.5 0 0 1 .708 0L7 1.293l.646-.647a.5.5 0 0 1 .708 0L9 1.293l.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .801.13l.5 1A.5.5 0 0 1 15 2v12a.5.5 0 0 1-.053.224l-.5 1a.5.5 0 0 1-.8.13L13 14.707l-.646.647a.5.5 0 0 1-.708 0L11 14.707l-.646.647a.5.5 0 0 1-.708 0L9 14.707l-.646.647a.5.5 0 0 1-.708 0L7 14.707l-.646.647a.5.5 0 0 1-.708 0L5 14.707l-.646.647a.5.5 0 0 1-.708 0L3 14.707l-.646.647a.5.5 0 0 1-.801-.13l-.5-1A.5.5 0 0 1 1 14V2a.5.5 0 0 1 .053-.224l.5-1a.5.5 0 0 1 .367-.27zm.217 1.338L2 2.118v11.764l.137.274.51-.51a.5.5 0 0 1 .707 0l.646.647.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.646.646.646-.646a.5.5 0 0 1 .708 0l.509.509.137-.274V2.118l-.137-.274-.51.51a.5.5 0 0 1-.707 0L12 1.707l-.646.647a.5.5 0 0 1-.708 0L10 1.707l-.646.647a.5.5 0 0 1-.708 0L8 1.707l-.646.647a.5.5 0 0 1-.708 0L6 1.707l-.646.647a.5.5 0 0 1-.708 0L4 1.707l-.646.647a.5.5 0 0 1-.708 0l-.509-.51z"
                                    />
                                    <path
                                        d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm8-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5z"
                                    />
                                    </svg>
                                    <div class="mx-2">Resep</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" onclick="log()">
                                <div class="d-flex flex-row align-items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                                    <path
                                        d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"
                                    />
                                    <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z" />
                                    <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z" />
                                    </svg>
                                    <div class="mx-2">Log</div>
                                </div>
                            </a>
                        </li>
                        </ul>
                    </div>
                    <div class="d-flex flex-column mt-5" id="sb-summary-ctn">
                        <div class="fw-light w-100 text-center border-top border-bottom border-dark text-white bg-dark">Steambox summary</div>
                    </div>
                </nav>
                <div class="d-flex flex-row col-12">
                    <div class="col-2 col-xl-2 col-xxl-1"></div>
                    <main class="col-md-10 col-lg-10 col-xl-10 col-xxl-11">
                        <div id="content"></div>
                    </main>
                </div>
            </div>
        </div>

        <div class="d-none justify-content-between align-items-center border-top border-white" id="sb-status-item">
            <div class="p-1 text-white fs-4 fw-bold">1</div>
            <div class="pe-1 text-end">Idle</div>
        </div>

        <script type="text/javascript" src="js/jquery-3.6.0.min.js?n=1"></script>
        <script lang="javascript" src="./ext-modules/node_modules/xlsx/dist/xlsx.full.min.js?n=1"></script>
        <script type="text/javascript" src="./ext-modules/FileSaver.js-master/dist/FileSaver.min.js?n=1"></script>
        <script type="text/javascript" src="bootstrap-5.1.3-dist/js/bootstrap.js?n=1"></script>
        <script type="text/javascript" src="js/main.js?n=1"></script>
        <script type="text/javascript" src="js/date-format.js?n=1"></script>
        <script type="text/javascript" src="js/page-main.js?n=3"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                getMesinData();
                $(".nav li a").click(function (e) {
                    $(".nav li a.active").removeClass("active");

                    var $parent = $(this);
                    $parent.addClass("active");
                    e.preventDefault();
                });
            });
        </script>
        <?php
        if (isset($_SESSION['username'])){
            $role = $_SESSION['role'];
            $username = $_SESSION['username'];
            
            echo '<script type="text/javascript">user_role = "'.$role.'";</script>';
            echo '<script type="text/javascript">applyUSERNAME("'.$username.'");</script>';
            if($role == 'Operator'){
                echo '<script type="text/javascript">displayTypeOperator();</script>';
            }
            else if($role == 'Admin'){
                echo '<script type="text/javascript">displayTypeAdmin();</script>';
            }
        }
    ?>
    </body>
</html>

