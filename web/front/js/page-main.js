var map_task;
var map_task_today;
var map_history;
var map_history_current;
var map_steam;
var map_active_task;
var map_resep;
var map_user;
var map_color;
var map_recent;
var map_culikan_std;
var heating_std;
var max_qty_allowed;

function getMesinData() {
    console.log("heating_std");
    const url = "http://localhost/backend/postdm.php?action_id=4";
    $.ajax({
        url: url,
        type: "GET",
        success: function (response_json) {
            document.getElementById("home-container").innerHTML = "";
            map_task_today = [];

            map_task = response_json["value_task"];
            map_history = response_json["value_history"];
            map_steam = response_json["value_steam"];
            map_active_task = response_json["value_active_task"];
            map_resep = response_json["value_resep"];
            map_user = response_json["value_user"];
            map_color = response_json["value_color"];
            map_recent = response_json["value_recent"];
            map_culikan_std = response_json["value_culikan"];
            heating_std = parseInt(response_json["value_heating_std"][0]["heating_std"]);

            console.log(heating_std);

            var i = 0;
            map_steam.forEach(function (element) {
                const id = element["id"];

                const is_status_already = null != document.getElementById("steam-status-" + id);

                if (!is_status_already) {
                    const steam_status_ctn = document.getElementById("sb-status-item").cloneNode(true);
                    steam_status_ctn.classList.remove("d-none");
                    steam_status_ctn.classList.add("d-flex");
                    steam_status_ctn.id = "steam-status-" + id;

                    steam_status_ctn.children[0].innerHTML = element["label"];
                    steam_status_ctn.children[1].innerHTML = "Idle<br/>" + "Temperatur : " + "";
                    steam_status_ctn.children[0].classList.add("bg-dark");

                    document.getElementById("sb-summary-ctn").appendChild(steam_status_ctn);
                }

                if (isSteamActive(id)) {
                    const task = getActiveTaskByMachineId(id);
                    console.log("active id:" + id);

                    AssignTaskToCard(task);
                    i++;
                } else {
                    console.log("unactive id:" + id);
                    assignIdleSteamToCard(id);
                }
            });

            if (i > 0) {
                main_timer = setTimeout(refreshData, 1000);
            }

            map_task.forEach(function (element) {
                const id = element["id"].slice(0, 8);
                const date_now = new Date().format("yyyymmdd");

                if (id.localeCompare(date_now) == 0) {
                    map_task_today.push(element);
                }
            });

            /*
         map_active_task.forEach(function (element) {
            const task = getTaskByID(element["task_id"]);
            assignTasktoCard(task);
         });
         */
        },
        fail: function (error) {
            console.log("heating_stdd");
        },
    });
}

function isSteamActive(id) {
    var isActive = false;

    map_active_task.forEach(function (element) {
        const task = getTaskByID(element["task_id"]);
        const machine_id = task["mesin_id"];

        console.log("mesin id pada task: " + machine_id + " id mesin : " + id);

        if (machine_id == id) {
            isActive = true;
        }
    });

    return isActive;
}

function getActiveTaskByMachineId(id) {
    var a_task;

    map_active_task.forEach(function (element) {
        const task = getTaskByID(element["task_id"]);
        const machine_id = task["mesin_id"];

        if (machine_id == id) {
            a_task = task;
        }
    });

    return a_task;
}

function getNamaResepByID(id) {
    var ret_val = null;
    map_resep.forEach((element) => {
        const task_id = element["id"];
        if (id.localeCompare(task_id) == 0) {
            ret_val = element;
        }
    });
    if (null != ret_val) {
        return ret_val["nama"];
    } else {
        return "Resep Dihapus";
    }
}

function getAdditional(resep_nama) {
    var task,
        ret = 0;

    map_task_today.forEach(function (element) {
        const id = element["resep_id"];
        const nama = getNamaResepByID(id);

        if (nama.localeCompare(resep_nama) == 0) {
            task = element;
        }
    });

    if (null == task) {
        ret = 0;
    } else {
        task["additional"].split(",,,").forEach(function (element) {
            var value = parseInt(element);

            ret += value;
        });
    }

    return ret;
}

function getResepByName(name) {
    var ret_val;
    map_resep.forEach((element) => {
        const nama = element["nama"];
        if (name.localeCompare(nama) == 0) {
            ret_val = element;
        }
    });
    return ret_val;
}

function getTaskByID(id) {
    var ret_val;
    map_task.forEach((element) => {
        const task_id = element["id"];
        if (id.localeCompare(task_id) == 0) {
            ret_val = element;
        }
    });
    return ret_val;
}

function getRecentByTaskID(id) {
    var ret_val;
    map_recent.forEach((element) => {
        const task_id = element["task_id"];
        if (id.localeCompare(task_id) == 0) {
            ret_val = element;
        }
    });
    return ret_val;
}

function getHistoryByTaskID(id) {
    var ret_val;
    map_history.forEach((element) => {
        const task_id = element["task_id"];
        if (id.localeCompare(task_id) == 0) {
            ret_val = element;
        }
    });
    return ret_val;
}

function getSteamByID(id) {
    var ret_val;
    map_steam.forEach((element) => {
        const steam_id = element["id"];
        if (id.localeCompare(steam_id) == 0) {
            ret_val = element;
        }
    });
    return ret_val;
}

function assignIdleSteamToCard(id) {
    const parent = document.getElementById("home-container");
    const steam_status_ctn = document.getElementById("steam-status-" + id);
    const steam = getSteamByID(id);
    const idle_id = "idle-steam-id-" + id;

    var idle_ctn = document.getElementById("tab-mesin-idle").cloneNode(true);
    idle_ctn.classList.remove("d-none");
    idle_ctn.id = idle_id;

    parent.appendChild(idle_ctn);
    const idle = document.getElementById(idle_id);
    idle.children[0].id = "mesin-tab-idle-" + idle_id;
    idle.children[1].id = "mesin-tab-assign-" + idle_id;
    idle.children[0].children[0].id = "steam-id-" + idle_id;
    idle.children[0].children[0].value = id;

    idle.children[0].children[1].innerHTML = "Steambox - " + steam["label"];
    idle.children[0].children[2].innerHTML = "(address : " + steam["address"] + ")";
    idle.children[1].children[2].id = "forms-idle-" + idle_id;
    idle.children[1].children[2].children[2].id = "check-nmi-" + idle_id;
    idle.children[1].children[2].children[1].id = "troli-ctn-" + idle_id;
    idle.children[1].children[2].children[4].id = "additional-ctn-" + idle_id;
    idle.children[1].children[1].children[0].innerHTML = "Steambox - " + steam["label"];
    idle.children[1].children[2].children[5].children[0].id = "btn-terapkan-" + idle_id;
    idle.children[1].children[2].children[0].children[1].children[1].id = "dataListResep-" + idle_id;
    idle.children[1].children[2].children[1].children[1].children[0].id = "presetqty-" + idle_id;
    idle.children[1].children[2].children[2].children[1].id = "detail-barang-ctn-" + idle_id;
    idle.children[1].children[2].children[4].children[1].children[0].id = "additional-val-" + idle_id;
    idle.children[1].children[2].children[0].children[1].children[1].id = "dataListResep-" + idle_id;
    idle.children[1].children[2].children[0].children[1].children[0].id = "resep-val-" + idle_id;
    idle.children[1].children[2].children[2].children[2].id = "btn-tambah-item-" + idle_id;
    idle.children[1].children[2].children[2].children[3].children[0].id = "item-chk-enough-" + idle_id;
    idle.children[1].children[2].children[1].children[1].children[0];
    idle.children[1].children[2].children[1].children[2].children[1].children[0].id = "presetket-" + idle_id;

    steam_status_ctn.children[0].classList.remove("bg-success");
    steam_status_ctn.children[0].classList.add("bg-dark");
    steam_status_ctn.children[1].classList.remove("text-success");
    steam_status_ctn.children[1].classList.add("text-dark");
    steam_status_ctn.children[1].innerHTML = "Idle";

    idle.children[1].children[2].children[0].children[1].children[0].addEventListener(
        "change",
        function () {
            const val = idle.children[1].children[2].children[0].children[1].children[0].value;
            var is_exist = false;
            console.log(getAdditional(val));

            map_resep.forEach(function (element) {
                const nama = element["nama"];

                if (nama.localeCompare(val) == 0) {
                    is_exist = true;
                    console.log(nama);
                }
            });

            if (!is_exist) {
                idle.children[1].children[2].children[1].children[2].classList.remove("d-block");
                idle.children[1].children[2].children[1].children[2].classList.add("d-none");

                idle.children[1].children[2].children[1].classList.remove("d-block");
                idle.children[1].children[2].children[1].classList.add("d-none");

                idle.children[1].children[2].children[0].children[2].classList.remove("d-none");
                idle.children[1].children[2].children[0].children[2].classList.add("d-block");

                idle.children[1].children[2].children[2].classList.remove("d-block");
                idle.children[1].children[2].children[2].classList.add("d-none");

                idle.children[1].children[2].children[4].classList.remove("d-block");
                idle.children[1].children[2].children[4].classList.add("d-none");

                idle.children[1].children[2].children[5].children[0].classList.remove("d-block");
                idle.children[1].children[2].children[5].children[0].classList.add("d-none");
            } else if (is_exist) {
                document.getElementById("additional-val-" + idle_id).value = getAdditional(val);
                //getAdditional
                idle.children[1].children[2].children[1].classList.remove("d-none");
                idle.children[1].children[2].children[1].classList.add("d-block");

                idle.children[1].children[2].children[1].children[2].classList.remove("d-block");
                idle.children[1].children[2].children[1].children[2].classList.add("d-none");
            }
        },
        false
    );
    idle.children[1].children[2].children[1].children[1].children[0].addEventListener(
        "change",
        function () {
            const val = idle.children[1].children[2].children[1].children[1].children[0].value;

            const isDefault = val.localeCompare("default") == 0;
            const isCulikan = val.localeCompare("troli_culikan") == 0;
            const isCustom = val.localeCompare("troli_custom") == 0;

            if (!isDefault) {
                const nama_resep = idle.children[1].children[2].children[0].children[1].children[0].value;
                if (isCustom) {
                    max_qty_allowed = 1;
                    const parentCtn = document.getElementById("detail-barang-ctn-" + idle_id);
                    parentCtn.innerHTML = "";

                    idle.children[1].children[2].children[1].children[2].classList.remove("d-none");
                    idle.children[1].children[2].children[1].children[2].classList.add("d-block");

                    document.getElementById("presetket-" + idle_id).value = "default";
                } else {
                    max_qty_allowed = getResepByName(nama_resep)[val];

                    addAssignItem(idle_id, max_qty_allowed);

                    idle.children[1].children[2].children[1].children[2].classList.remove("d-block");
                    idle.children[1].children[2].children[1].children[2].classList.add("d-none");

                    idle.children[1].children[2].children[0].children[2].classList.remove("d-block");
                    idle.children[1].children[2].children[0].children[2].classList.add("d-none");

                    idle.children[1].children[2].children[2].classList.remove("d-none");
                    idle.children[1].children[2].children[2].classList.add("d-block");

                    idle.children[1].children[2].children[4].classList.remove("d-none");
                    idle.children[1].children[2].children[4].classList.add("d-block");

                    idle.children[1].children[2].children[5].children[0].classList.remove("d-none");
                    idle.children[1].children[2].children[5].children[0].classList.add("d-block");
                }
            }
        },
        false
    );
    idle.children[1].children[2].children[1].children[2].children[1].children[0].addEventListener(
        "change",
        function () {
            const val = idle.children[1].children[2].children[1].children[2].children[1].children[0].value;

            const isDefault = val.localeCompare("default") == 0;
            const isCulikan = val.localeCompare("ket_culikan") == 0;
            const isCustom = val.localeCompare("ket_sisa") == 0 || val.localeCompare("ket_trial") == 0;

            if (!isDefault) {
                const nama_resep = idle.children[1].children[2].children[0].children[1].children[0].value;
                if (isCulikan) {
                    max_qty_allowed = 1;
                    const parentCtn = document.getElementById("detail-barang-ctn-" + idle_id);
                    parentCtn.innerHTML = "";
                } else {
                    const parentCtn = document.getElementById("detail-barang-ctn-" + idle_id);
                    parentCtn.innerHTML = "";
                    max_qty_allowed = 1000000;
                }

                addAssignItem(idle_id, max_qty_allowed);

                idle.children[1].children[2].children[0].children[2].classList.remove("d-block");
                idle.children[1].children[2].children[0].children[2].classList.add("d-none");

                idle.children[1].children[2].children[2].classList.remove("d-none");
                idle.children[1].children[2].children[2].classList.add("d-block");

                idle.children[1].children[2].children[4].classList.remove("d-none");
                idle.children[1].children[2].children[4].classList.add("d-block");

                idle.children[1].children[2].children[5].children[0].classList.remove("d-none");
                idle.children[1].children[2].children[5].children[0].classList.add("d-block");
            }
        },
        false
    );
    idle.children[1].children[2].children[5].children[0].addEventListener(
        "click",
        function () {
            validate(idle_id, false);
        },
        false
    );
    idle.children[1].children[2].children[2].children[2].addEventListener(
        "click",
        function () {
            addAssignItem(idle_id);
        },
        false
    );
    idle.children[0].children[4].addEventListener(
        "click",
        function () {
            if (null == map_resep) {
                $("#pr-py-ok").click(function () {
                    $("#modal-pr-py").modal("hide");
                });

                $("#modal-pr-py").modal("show");
                $("#pr-py-msg").html("Buat data resep terlebih dahulu");
            } else {
                if (map_resep.length > 0) {
                    assignTask(idle_id);
                } else {
                    $("#pr-py-ok").click(function () {
                        $("#modal-pr-py").modal("hide");
                    });

                    $("#modal-pr-py").modal("show");
                    $("#pr-py-msg").html("Buat data resep terlebih dahulu");
                }
            }
        },
        false
    );
    idle.children[1].children[2].children[5].children[1].addEventListener(
        "click",
        function () {
            assignTask(idle_id);
        },
        false
    );
    idle.children[1].children[2].children[2].children[3].children[0].addEventListener(
        "click",
        function () {
            const parentCtn = document.getElementById("detail-barang-ctn-" + idle_id);
            const child = parentCtn.childElementCount;
            var i = 0;

            if (document.getElementById("item-chk-enough-" + idle_id).checked) {
                document.getElementById("btn-tambah-item-" + idle_id).classList.add("disabled");

                for (i = 0; i < child; i++) {
                    if (null != parentCtn.children[i]) {
                        const id_to_disabled = "delete-assign-id-" + idle_id + (i + 1);
                        document.getElementById(id_to_disabled).classList.add("disabled");
                    }
                }
            } else {
                document.getElementById("btn-tambah-item-" + idle_id).classList.remove("disabled");

                for (i = 0; i < child; i++) {
                    if (null != parentCtn.children[i]) {
                        const id_to_disabled = "delete-assign-id-" + idle_id + (i + 1);
                        document.getElementById(id_to_disabled).classList.remove("disabled");
                    }
                }
            }
        },
        false
    );
    document.getElementById("btn-submit-psq").addEventListener(
        "click",
        function () {
            validate(idle_id, true);
        },
        false
    );
}

function refreshData() {
    console.log("refresh");
    const url = "http://localhost/backend/postdm.php?action_id=4";
    $.ajax({
        url: url,
        type: "GET",
        success: function (response_json) {
            map_task = response_json["value_task"];
            map_history = response_json["value_history"];
            map_active_task = response_json["value_active_task"];
            map_recent = response_json["value_recent"];

            var i = 0;
            map_active_task.forEach(function (element) {
                const task = getTaskByID(element["task_id"]);
                maintainData(task);

                i++;
            });

            if (i == 0) {
                getMesinData();
            } else {
                main_timer = setTimeout(refreshData, 1000);
            }

            /*
        map_active_task.forEach(function (element) {
           const task = getTaskByID(element["task_id"]);
           assignTasktoCard(task);
        });
        */
        },
        fail: function (error) {},
    });
}

function maintainData(task) {
    console.log("new maintain data for task id : " + task["id"]);
    const steam = getSteamByID(task["mesin_id"]);
    const steam_status_ctn = document.getElementById("steam-status-" + task["mesin_id"]);
    const active_id = task["id"];
    const history = getHistoryByTaskID(active_id);
    const recent = getRecentByTaskID(active_id);

    const task_ctn = document.getElementById("task-id-" + active_id);
    const nama_resep = getNamaResepByID(task["resep_id"]);
    const resep = getResepByName(nama_resep);
    const items = task["items"].split(",,,");
    const lots = task["lots"].split(",,,");
    const additional = task["additional"].split(",,,");
    const colors = task["colors"].split(",,,");
    const heating_time = history["heating_timestamp"];
    const cooking_time = history["cooking_timestamp"];

    const item_ctn = document.getElementById("mesin-mli-hor-ctn");
    var item_ctn_clone;

    const table_item = document.getElementById("cw-item-ctn");

    var table_item_clone = table_item.cloneNode(true);
    table_item_clone.children[0].innerHTML = "Jam Mulai Pemanasan";
    table_item_clone.children[1].innerHTML = getTimeStr(false, heating_time);

    document.getElementById("txt-pv-" + active_id).innerHTML = recent["pv"] + " " + "<sup>o</sup>C";

    document.getElementById("ctn-history-" + active_id).innerHTML = "";
    document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);

    if (cooking_time == 0) {
        document.getElementById("txt-status-" + active_id).innerHTML = "HEATING";
        steam_status_ctn.children[1].innerHTML = "Pemanasan";

        for (i = 0; i < additional.length; i++) {
            table_item_clone = table_item.cloneNode(true);
            table_item_clone.children[0].innerHTML = "Tambahan waktu";
            table_item_clone.children[1].innerHTML = additional[i] + " mnt";

            if (i == additional.length - 1) {
                table_item_clone.classList.remove("border-bottom");
                table_item_clone.classList.add("mb-2");
            }

            document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);
        }
    } else {
        document.getElementById("txt-status-" + active_id).innerHTML = "COOKING";
        table_item_clone = table_item.cloneNode(true);
        table_item_clone.children[0].innerHTML = "Durasi Pemanasan";
        table_item_clone.children[1].innerHTML = getDuration(heating_time, cooking_time);

        document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);

        table_item_clone = table_item.cloneNode(true);
        table_item_clone.children[0].innerHTML = "Jam Mulai Memasak";
        table_item_clone.children[1].innerHTML = getTimeStr(false, cooking_time);

        document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);

        var total_additional = parseInt(resep["durasi"]);

        for (i = 0; i < additional.length; i++) {
            total_additional += parseInt(additional[i]);
            table_item_clone = table_item.cloneNode(true);
            table_item_clone.children[0].innerHTML = "Tambahan waktu";
            table_item_clone.children[1].innerHTML = additional[i] + " mnt";

            document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);
        }

        table_item_clone = table_item.cloneNode(true);
        table_item_clone.children[0].innerHTML = "Jam Selesai (estimasi)";
        table_item_clone.children[1].innerHTML = estimateEnd(false, cooking_time, total_additional);
        table_item_clone.classList.remove("border-bottom");
        table_item_clone.classList.add("mb-2");

        steam_status_ctn.children[1].innerHTML = "Pemasakan <br/>" + "Estimasi selesai : " + estimateEnd(true, cooking_time, total_additional);

        document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);
    }

    if (recent["is_latch"] == 1) {
        document.getElementById("txt-steam1on-" + active_id).innerHTML = "ON";
        document.getElementById("txt-steam1on-" + active_id).className = "fw-bold p-1 col-10 bg-success text-end text-white fs-5";
    } else {
        document.getElementById("txt-steam1on-" + active_id).innerHTML = "OFF";
        document.getElementById("txt-steam1on-" + active_id).className = "fw-bold p-1 col-10 bg-warning text-end fs-5";
    }
    if (recent["is_latch_2"] == 1) {
        document.getElementById("txt-steam2on-" + active_id).innerHTML = "ON";
        document.getElementById("txt-steam2on-" + active_id).className = "fw-bold p-1 col-10 bg-success text-end text-white fs-5";
    } else {
        document.getElementById("txt-steam2on-" + active_id).innerHTML = "OFF";
        document.getElementById("txt-steam2on-" + active_id).className = "fw-bold p-1 col-10 bg-warning text-end fs-5";
    }
    if (!isTimeOut(recent["ts"])) {
        document.getElementById("txt-conn-" + active_id).innerHTML = "LIVE";
        document.getElementById("txt-conn-" + active_id).className = "col-12 bg-success text-white fs-5 fw-bold text-end pe-1 pt-2 pb-2";
    } else {
        document.getElementById("txt-conn-" + active_id).innerHTML = "DISCONNECT";
        document.getElementById("txt-conn-" + active_id).className = "col-12 bg-warning fs-5 fw-bold text-end pe-1 pt-2 pb-2";
    }
}

function MaintainData(task) {
    console.log("new maintain data for task id : " + task["id"]);
    const steam = getSteamByID(task["mesin_id"]);
    const history = getHistoryByTaskID(task["id"]);
    const recent = getRecentByTaskID(task["id"]);

    const task_ctn = document.getElementById("task-id-" + task["id"]);
    const parent_item_ctn = task_ctn.children[0].children[8];
    const nama_resep = getNamaResepByID(task["resep_id"]);
    const resep = getResepByName(nama_resep);
    const items = task["items"].split(",,,");
    const lots = task["lots"].split(",,,");
    const additional = task["additional"].split(",,,");
    const colors = task["colors"].split(",,,");
    const heating_time = history["heating_timestamp"];
    const cooking_time = history["cooking_timestamp"];

    const item_ctn = document.getElementById("mesin-mli-ctn");
    const table = task_ctn.children[0].children[12].children[0].children[1];

    table.innerHTML = "";

    var row = table.insertRow(0);
    row.insertCell(0).innerHTML = "Jam Mulai Pemanasan";
    row.insertCell(1).innerHTML = getTimeStr(false, heating_time);
    row.insertCell(2).innerHTML = "-";

    var i = 1;

    task_ctn.children[0].children[0].innerHTML = "Steambox - " + steam["label"];
    task_ctn.children[0].children[1].children[0].innerHTML = "(address : " + steam["address"] + ")";
    task_ctn.children[0].children[7].innerHTML = nama_resep;
    task_ctn.children[0].children[3].innerHTML = recent["pv"];

    if (cooking_time == 0) {
        task_ctn.children[0].children[5].innerHTML = "HEATING";

        for (i = 1; i < additional.length + 1; i++) {
            row = table.insertRow(i);
            row.insertCell(0).innerHTML = "Tambahan waktu";
            row.insertCell(1).innerHTML = "-";
            row.insertCell(2).innerHTML = additional[i - 1] + " mnt";
        }
    } else {
        task_ctn.children[0].children[5].innerHTML = "COOKING";

        row = table.insertRow(1);
        row.insertCell(0).innerHTML = "Durasi Pemanasan";
        row.insertCell(1).innerHTML = "";
        row.insertCell(2).innerHTML = getDuration(heating_time, cooking_time);

        row = table.insertRow(2);
        row.insertCell(0).innerHTML = "Jam Mulai Memasak";
        row.insertCell(1).innerHTML = getTimeStr(false, cooking_time);
        row.insertCell(2).innerHTML = "-";

        var total_additional = parseInt(resep["durasi"]);

        for (i = 3; i < additional.length + 3; i++) {
            total_additional += parseInt(additional[i - 3]);
            row = table.insertRow(i);
            row.insertCell(0).innerHTML = "Tambahan waktu - " + (i - 2);
            row.insertCell(1).innerHTML = "-";
            row.insertCell(2).innerHTML = additional[i - 3] + " mnt";
        }

        row = table.insertRow(3 + additional.length);
        row.insertCell(0).innerHTML = "Jam Selesai (estimasi)";
        row.insertCell(1).innerHTML = estimateEnd(false, cooking_time, total_additional);
        row.insertCell(2).innerHTML = "-";

        $("#task-countdown-" + task["id"]).show();
        task_ctn.children[0].children[1].children[1].innerHTML = "Estimasi selesai : " + estimateEnd(true, cooking_time, total_additional);
    }
    if (recent["is_latch"] == 1) {
        task_ctn.children[0].children[10].children[0].children[1].innerHTML = "LATCH";
        task_ctn.children[0].children[10].children[0].children[1].className = "col-6 bg-success mx-3 text-center text-white";
    } else {
        task_ctn.children[0].children[10].children[0].children[1].innerHTML = "DISENGAGE";
        task_ctn.children[0].children[10].children[0].children[1].className = "col-6 bg-danger mx-3 text-center text-white";
    }
    if (!isTimeOut(recent["ts"])) {
        task_ctn.children[0].children[10].children[1].children[1].innerHTML = "LIVE";
        task_ctn.children[0].children[10].children[1].children[1].className = "col-6 bg-success mx-3 text-center text-white";
    } else {
        task_ctn.children[0].children[10].children[1].children[1].innerHTML = "DISCONNECT";
        task_ctn.children[0].children[10].children[1].children[1].className = "col-6 bg-danger mx-3 text-center text-white";
    }

    if (items.length == 1) {
        const str = lots[0] + " / " + colors[0] + " / " + items[0] + " Gld";
        item_ctn.children[1].innerHTML = str;
    } else {
        parent_item_ctn.innerHTML = "";
        /*
        const str = lots[0] + " / " + colors[0] + " / " + items[0] + " Box";
        item_ctn.children[1].innerHTML = str;

        */
        for (i = 0; i < items.length - 1; i++) {
            const child = item_ctn.cloneNode(true);

            const str = lots[i] + " / " + colors[i] + " / " + items[i] + " Gld";

            child.children[0].innerHTML = "Item " + (i + 1) + ":";
            child.children[1].innerHTML = str;

            parent_item_ctn.appendChild(child);
        }
    }
}

function AssignTaskToCard(task) {
    const parent = document.getElementById("home-container");
    const steam_status_ctn = document.getElementById("steam-status-" + task["mesin_id"]);
    const steam = getSteamByID(task["mesin_id"]);
    const active_id = task["id"];
    const history = getHistoryByTaskID(active_id);
    const recent = getRecentByTaskID(active_id);

    var active_ctn = document.getElementById("mesin-hor-tab").cloneNode(true);
    var modal = document.getElementById("exampleModal").cloneNode(true);

    active_ctn.classList.remove("d-none");
    active_ctn.classList.add("d-flex");
    active_ctn.id = "task-id-" + task["id"];
    modal.id = "modal-task-id-" + task["id"];

    parent.appendChild(active_ctn);
    parent.appendChild(modal);

    const task_ctn = document.getElementById("task-id-" + active_id);
    const modal_ctn = document.getElementById("modal-task-id-" + active_id);

    const nama_resep = getNamaResepByID(task["resep_id"]);
    const resep = getResepByName(nama_resep);
    const items = task["items"].split(",,,");
    const lots = task["lots"].split(",,,");
    const additional = task["additional"].split(",,,");
    const colors = task["colors"].split(",,,");
    const heating_time = history["heating_timestamp"];
    const cooking_time = history["cooking_timestamp"];

    const item_ctn = document.getElementById("mesin-mli-hor-ctn");
    var item_ctn_clone;

    const table_item = document.getElementById("cw-item-ctn");

    var table_item_clone = table_item.cloneNode(true);
    table_item_clone.children[0].innerHTML = "Jam Mulai Pemanasan";
    table_item_clone.children[1].innerHTML = getTimeStr(false, heating_time);

    task_ctn.children[0].children[0].children[0].innerHTML = steam["label"];
    task_ctn.children[1].children[1].children[1].innerHTML = nama_resep;
    task_ctn.children[1].children[4].children[2].id = "ctn-history-" + active_id;
    task_ctn.children[1].children[0].children[1].children[0].id = "txt-status-" + active_id;
    task_ctn.children[1].children[3].children[0].children[2].children[0].children[1].id = "txt-steam1on-" + active_id;
    task_ctn.children[1].children[3].children[0].children[2].children[1].children[1].id = "txt-steam2on-" + active_id;
    task_ctn.children[1].children[3].children[1].children[2].children[0].id = "txt-conn-" + active_id;
    task_ctn.children[1].children[0].children[1].children[1].children[1].id = "txt-pv-" + active_id;
    document.getElementById("txt-pv-" + active_id).innerHTML = recent["pv"] + " <sup>o</sup>C";

    document.getElementById("ctn-history-" + active_id).innerHTML = "";
    document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);

    steam_status_ctn.children[0].classList.remove("bg-dark");
    steam_status_ctn.children[0].classList.add("bg-success");
    steam_status_ctn.children[1].classList.remove("text-dark");
    steam_status_ctn.children[1].classList.add("text-success");
    steam_status_ctn.children[1].innerHTML = "On Going";

    task_ctn.children[1].children[2].children[1].innerHTML = "";
    for (i = 0; i < items.length - 1; i++) {
        console.log(items);
        item_ctn_clone = item_ctn.cloneNode(true);
        item_ctn_clone.classList.remove("d-none");
        item_ctn_clone.classList.add("d-flex");

        const str = lots[i] + " / " + colors[i] + " / " + items[i] + " Gld";

        item_ctn_clone.children[0].innerHTML = "Item " + (i + 1) + " :";
        item_ctn_clone.children[1].innerHTML = str;

        task_ctn.children[1].children[2].children[1].appendChild(item_ctn_clone);
    }

    if (cooking_time == 0) {
        document.getElementById("txt-status-" + active_id).innerHTML = "HEATING";

        for (i = 0; i < additional.length; i++) {
            table_item_clone = table_item.cloneNode(true);
            table_item_clone.children[0].innerHTML = "Tambahan waktu";
            table_item_clone.children[1].innerHTML = additional[i] + " mnt";

            if (i == additional.length - 1) {
                table_item_clone.classList.remove("border-bottom");
                table_item_clone.classList.add("mb-2");
            }

            document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);
        }
    } else {
        document.getElementById("txt-status-" + active_id).innerHTML = "COOKING";
        table_item_clone = table_item.cloneNode(true);
        table_item_clone.children[0].innerHTML = "Durasi Pemanasan";
        table_item_clone.children[1].innerHTML = getDuration(heating_time, cooking_time);

        document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);

        table_item_clone = table_item.cloneNode(true);
        table_item_clone.children[0].innerHTML = "Jam Mulai Memasak";
        table_item_clone.children[1].innerHTML = getTimeStr(false, cooking_time);

        document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);

        var total_additional = parseInt(resep["durasi"]);

        for (i = 0; i < additional.length; i++) {
            total_additional += parseInt(additional[i]);
            table_item_clone = table_item.cloneNode(true);
            table_item_clone.children[0].innerHTML = "Tambahan waktu";
            table_item_clone.children[1].innerHTML = additional[i] + " mnt";

            document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);
        }

        table_item_clone = table_item.cloneNode(true);
        table_item_clone.children[0].innerHTML = "Jam Selesai (estimasi)";
        table_item_clone.children[1].innerHTML = estimateEnd(false, cooking_time, total_additional);
        table_item_clone.classList.remove("border-bottom");
        table_item_clone.classList.add("mb-2");

        document.getElementById("ctn-history-" + active_id).appendChild(table_item_clone);
    }

    if (recent["is_latch"] == 1) {
        document.getElementById("txt-steam1on-" + active_id).innerHTML = "ON";
        document.getElementById("txt-steam1on-" + active_id).className = "fw-bold p-1 col-10 bg-success text-end text-white fs-5";
    } else {
        document.getElementById("txt-steam1on-" + active_id).innerHTML = "OFF";
        document.getElementById("txt-steam1on-" + active_id).className = "fw-bold p-1 col-10 bg-warning text-end fs-5";
    }
    if (recent["is_latch_2"] == 1) {
        document.getElementById("txt-steam2on-" + active_id).innerHTML = "ON";
        document.getElementById("txt-steam2on-" + active_id).className = "fw-bold p-1 col-10 bg-success text-end text-white fs-5";
    } else {
        document.getElementById("txt-steam2on-" + active_id).innerHTML = "OFF";
        document.getElementById("txt-steam2on-" + active_id).className = "fw-bold p-1 col-10 bg-warning text-end fs-5";
    }
    if (!isTimeOut(recent["ts"])) {
        document.getElementById("txt-conn-" + active_id).innerHTML = "LIVE";
        document.getElementById("txt-conn-" + active_id).className = "col-12 bg-success text-white fs-5 fw-bold text-end pe-1 pt-2 pb-2";
    } else {
        document.getElementById("txt-conn-" + active_id).innerHTML = "DISCONNECT";
        document.getElementById("txt-conn-" + active_id).className = "col-12 bg-warning fs-5 fw-bold text-end pe-1 pt-2 pb-2";
    }

    task_ctn.addEventListener(
        "click",
        function () {
            $("#modal-task-id-" + task["id"]).modal("show");
        },
        false
    );

    $("#modal-task-id-" + task["id"]).on("hidden.bs.modal", function (e) {
        modal_ctn.children[0].children[0].children[2].classList.remove("d-none");
        modal_ctn.children[0].children[0].children[2].classList.add("d-block");

        modal_ctn.children[0].children[0].children[3].classList.remove("d-block");
        modal_ctn.children[0].children[0].children[3].classList.add("d-none");

        modal_ctn.children[0].children[0].children[4].classList.remove("d-block");
        modal_ctn.children[0].children[0].children[4].classList.add("d-none");
    });

    modal_ctn.children[0].children[0].children[1].innerHTML = "Steambox - " + steam["label"];
    modal_ctn.children[0].children[0].children[2].children[0].children[0].children[0].addEventListener(
        "click",
        function () {
            modal_ctn.children[0].children[0].children[2].classList.remove("d-block");
            modal_ctn.children[0].children[0].children[2].classList.add("d-none");

            modal_ctn.children[0].children[0].children[3].classList.remove("d-none");
            modal_ctn.children[0].children[0].children[3].classList.add("d-block");

            //clearTimeout(main_timer);
        },
        false
    );
    modal_ctn.children[0].children[0].children[3].children[1].children[0].children[0].addEventListener(
        "click",
        function () {
            $.ajax({
                type: "POST", //type of method
                url: "http://localhost/backend/postdm.php", //your page
                data: { action_id: 9, task_id: task["id"] }, // passing the values
                success: function (res) {
                    document.getElementById("a-home").click();
                    $("#modal-task-id-" + task["id"]).modal("hide");
                },
                fail: function (onfail) {
                    document.getElementById("a-home").click();
                },
            });
        },
        false
    );
    modal_ctn.children[0].children[0].children[2].children[0].children[1].children[0].addEventListener(
        "click",
        function () {
            modal_ctn.children[0].children[0].children[2].classList.remove("d-block");
            modal_ctn.children[0].children[0].children[2].classList.add("d-none");

            modal_ctn.children[0].children[0].children[4].classList.remove("d-none");
            modal_ctn.children[0].children[0].children[4].classList.add("d-block");

            //clearTimeout(main_timer);
        },
        false
    );
    modal_ctn.children[0].children[0].children[4].children[0].children[2].addEventListener(
        "click",
        function () {
            "use strict";

            var forms = document.querySelectorAll(".needs-validation");

            Array.prototype.slice.call(forms).forEach(function (form) {
                form.addEventListener(
                    "submit",
                    function (event) {
                        if (!form.checkValidity()) {
                            event.preventDefault();
                            event.stopPropagation();
                        } else {
                            //0 0 4 0 1 0
                            const add_time_value = modal_ctn.children[0].children[0].children[4].children[0].children[1].children[0].value;
                            $.ajax({
                                type: "POST", //type of method
                                url: "http://localhost/backend/postdm.php", //your page
                                data: { action_id: 10, time_value: add_time_value, task_id: task["id"] }, // passing the values
                                success: function (res) {
                                    document.getElementById("a-home").click();
                                    $("#modal-task-id-" + task["id"]).modal("hide");
                                },
                                fail: function (onfail) {
                                    home();
                                },
                            });
                            //submitTask(id);
                        }

                        event.preventDefault();

                        form.classList.remove("needs-validation");
                        form.classList.add("was-validated");
                    },
                    false
                );
            });
        },
        false
    );
}

function assignLogToTable(map) {
    map_history_current = map;
    const table = document.getElementById("tabel-log");
    const parent_ctn = document.getElementById("table-log");

    parent_ctn.innerHTML = "";

    var index = 0;

    map.forEach(function (element) {
        const item_ctn = document.getElementById("table-item").cloneNode(true);
        item_ctn.classList.remove("d-none");
        item_ctn.classList.add("d-flex");

        const heating_time = element["heating_timestamp"];
        const cooking_time = element["cooking_timestamp"];
        const finish_time = element["finish_timestamp"];
        const isAborted = element["remark"].localeCompare("ABORTED") == 0;

        console.log("is aborted = " + isAborted);

        console.log(heating_time + "   " + cooking_time);

        const task = getTaskByID(element["task_id"]);
        const nama_resep = getNamaResepByID(task["resep_id"]);
        const resep = getResepByName(nama_resep);
        console.log("std_cook_min : " + nama_resep);
        const cooking_std_min = resep["std_cook_min"];
        const cooking_std_max = resep["std_cook_max"];

        var batch = "";
        var add_time = 0;
        const batch_split = task["lots"].split(",,,");
        const qty_split = task["items"].split(",,,");

        for (index = 0; index < batch_split.length - 1; index++) {
            batch += "Batch : " + batch_split[index] + "<br />" + "Qty : " + qty_split[index] + " gld<br />";
        }

        task["additional"].split(",,,").forEach(function (split) {
            const int_val = parseInt(split);
            add_time += int_val;
        });

        const pemanasan = cooking_time == 0 ? "" : "<br/>Durasi : " + getDuration(heating_time, cooking_time);
        const pemasakan = cooking_time == 0 ? "-" : finish_time == 0 ? "-" : getTimeStr(true, cooking_time) + "<br/>" + "Durasi : " + getDuration(cooking_time, finish_time);
        var durasi_str = "";

        if (isAborted) {
            item_ctn.children[6].className = "bg-warning text-white col-2 fs-6 fw-light align-items-center d-flex justify-content-center text-center";
        } else {
            console.log(getDuration(cooking_time, finish_time) + "," + cooking_std_max);
            if (isAboveLimit(getDuration(cooking_time, finish_time), cooking_std_max)) {
                item_ctn.children[6].className = "bg-danger text-white col-2 fs-6 fw-light align-items-center d-flex justify-content-center text-center";
            } else if (isBelowLimit(getDuration(cooking_time, finish_time), cooking_std_min)) {
                item_ctn.children[6].className = "bg-success text-white col-2 fs-6 fw-light align-items-center d-flex justify-content-center text-center";
            } else {
                item_ctn.children[6].className = "bg-white text-dark col-2 fs-6 fw-light align-items-center d-flex justify-content-center text-center";
            }
        }

        item_ctn.children[0].innerHTML = getTimeStr(true, element["heating_timestamp"]).split(" ")[0];
        item_ctn.children[2].innerHTML = getNamaResepByID(task["resep_id"]) + "/<br/>" + task["troli"];
        item_ctn.children[3].innerHTML = batch;
        item_ctn.children[4].innerHTML = add_time + " menit";
        item_ctn.children[5].innerHTML = getTimeStr(true, heating_time) + pemanasan;
        item_ctn.children[6].innerHTML = pemasakan;
        item_ctn.children[7].innerHTML = task["pic"];
        item_ctn.children[8].innerHTML = element["remark"];
        parent_ctn.appendChild(item_ctn);

        /*
        <th scope="col">No</th>
        <th scope="col">Date</th>
        <th scope="col">Nama Resep</th>
        <th scope="col">Batch/Qty</th>
        <th scope="col">Add Time</th>
        <th scope="col">Start</th>
        <th scope="col">Finish</th>
        <th scope="col">PIC</th>
        <th scope="col">Remark</th>
       */
        /*

       const cell_no = row.insertCell(0);
       const cell_date = row.insertCell(1);
       const cell_resep = row.insertCell(2);
       const cell_batch = row.insertCell(3);
       const cell_add = row.insertCell(4);
       const cell_start = row.insertCell(5);
       const cell_finish = row.insertCell(6);
       const cell_pic = row.insertCell(7);
       const cell_remark = row.insertCell(8);

       cell_no.innerHTML = index + 1;
       cell_date.innerHTML = getTimeStr(true, element["heating_timestamp"]).split(" ")[0];
       cell_resep.innerHTML = getNamaResepByID(task["resep_id"]);
       cell_batch.innerHTML = batch;
       cell_add.innerHTML = add_time + " menit";
       cell_start.innerHTML = getTimeStr(true, element["heating_timestamp"]).split(" ")[1];
       cell_finish.innerHTML = getTimeStr(true, element["finish_timestamp"]).split(" ")[1];
       cell_pic.innerHTML = task["pic"];
       cell_remark.innerHTML = element["remark"];
       */
    });
}

function assignTasktoCard(task) {
    const parent = document.getElementById("home-container");
    const steam = getSteamByID(task["mesin_id"]);
    const history = getHistoryByTaskID(task["id"]);
    const recent = getRecentByTaskID(task["id"]);

    var active_ctn = document.getElementById("mesin-tab").cloneNode(true);
    var modal = document.getElementById("exampleModal").cloneNode(true);

    active_ctn.classList.remove("d-none");
    active_ctn.id = "task-id-" + task["id"];
    modal.id = "modal-task-id-" + task["id"];

    parent.appendChild(active_ctn);
    parent.appendChild(modal);

    const task_ctn = document.getElementById("task-id-" + task["id"]);
    const modal_ctn = document.getElementById("modal-task-id-" + task["id"]);

    const parent_item_ctn = task_ctn.children[0].children[8];
    const nama_resep = getNamaResepByID(task["resep_id"]);
    const resep = getResepByName(nama_resep);
    const items = task["items"].split(",,,");
    const lots = task["lots"].split(",,,");
    const additional = task["additional"].split(",,,");
    const colors = task["colors"].split(",,,");
    const heating_time = history["heating_timestamp"];
    const cooking_time = history["cooking_timestamp"];
    //8.0
    const item_ctn = document.getElementById("mesin-mli-ctn");
    const table = task_ctn.children[0].children[12].children[0].children[1];

    var row = table.insertRow(0);
    row.insertCell(0).innerHTML = "Jam Mulai Pemanasan";
    row.insertCell(1).innerHTML = getTimeStr(false, heating_time);
    row.insertCell(2).innerHTML = "-";

    var i = 1;

    task_ctn.children[0].children[0].innerHTML = "Steambox - " + steam["label"];
    task_ctn.children[0].children[1].children[0].innerHTML = "(address : " + steam["address"] + ")";
    task_ctn.children[0].children[1].children[1].id = "task-countdown-" + task["id"];
    task_ctn.children[0].children[7].innerHTML = nama_resep;
    task_ctn.children[0].children[3].innerHTML = recent["pv"];

    $("#task-countdown-" + task["id"]).hide();

    if (cooking_time == 0) {
        task_ctn.children[0].children[5].innerHTML = "HEATING";

        for (i = 1; i < additional.length + 1; i++) {
            row = table.insertRow(i);
            row.insertCell(0).innerHTML = "Tambahan waktu";
            row.insertCell(1).innerHTML = "-";
            row.insertCell(2).innerHTML = additional[i - 1] + " mnt";
        }
    } else {
        task_ctn.children[0].children[5].innerHTML = "COOKING";

        row = table.insertRow(1);
        row.insertCell(0).innerHTML = "Durasi Pemanasan";
        row.insertCell(1).innerHTML = "";
        row.insertCell(2).innerHTML = getDuration(heating_time, cooking_time);

        row = table.insertRow(2);
        row.insertCell(0).innerHTML = "Jam Mulai Memasak";
        row.insertCell(1).innerHTML = getTimeStr(false, cooking_time);
        row.insertCell(2).innerHTML = "-";

        var total_additional = parseInt(resep["durasi"]);

        for (i = 3; i < additional.length + 3; i++) {
            total_additional += parseInt(additional[i - 3]);
            row = table.insertRow(i);
            row.insertCell(0).innerHTML = "Tambahan waktu - " + (i - 2);
            row.insertCell(1).innerHTML = "-";
            row.insertCell(2).innerHTML = additional[i - 3] + " mnt";
        }

        row = table.insertRow(3 + additional.length);
        row.insertCell(0).innerHTML = "Jam Selesai (estimasi)";
        row.insertCell(1).innerHTML = estimateEnd(false, cooking_time, total_additional);
        row.insertCell(2).innerHTML = "-";

        task_ctn.children[0].children[1].children[1].innerHTML = "Estimasi selesai : " + estimateEnd(true, cooking_time, total_additional);
    }

    if (cooking_time == 0) {
        task_ctn.children[0].children[5].innerHTML = "HEATING";
    } else {
        task_ctn.children[0].children[5].innerHTML = "COOKING";

        i = additional.length + 2;

        row = table.insertRow(i);
        row.insertCell(0).innerHTML = "Heating finish";
        row.insertCell(1).innerHTML = getTimeStr(false, cooking_time);
        row.insertCell(2).innerHTML = "-";

        i = i + 1;

        row = table.insertRow(i);
        row.insertCell(0).innerHTML = "Cooking start";
        row.insertCell(1).innerHTML = getTimeStr(false, cooking_time);
        row.insertCell(2).innerHTML = "-";
    }
    if (recent["is_latch"] == 1) {
        task_ctn.children[0].children[10].children[0].children[1].innerHTML = "LATCH";
        task_ctn.children[0].children[10].children[0].children[1].className = "col-6 bg-success mx-3 text-center text-white";
    } else {
        task_ctn.children[0].children[10].children[0].children[1].innerHTML = "DISENGAGE";
        task_ctn.children[0].children[10].children[0].children[1].className = "col-6 bg-danger mx-3 text-center text-white";
    }
    if (!isTimeOut(recent["ts"])) {
        task_ctn.children[0].children[10].children[1].children[1].innerHTML = "LIVE";
        task_ctn.children[0].children[10].children[1].children[1].className = "col-6 bg-success mx-3 text-center text-white";
    } else {
        task_ctn.children[0].children[10].children[1].children[1].innerHTML = "DISCONNECT";
        task_ctn.children[0].children[10].children[1].children[1].className = "col-6 bg-danger mx-3 text-center text-white";
    }

    task_ctn.addEventListener(
        "click",
        function () {
            $("#modal-task-id-" + task["id"]).modal("show");
        },
        false
    );
    $("#modal-task-id-" + task["id"]).on("hidden.bs.modal", function (e) {
        modal_ctn.children[0].children[0].children[2].classList.remove("d-none");
        modal_ctn.children[0].children[0].children[2].classList.add("d-block");

        modal_ctn.children[0].children[0].children[3].classList.remove("d-block");
        modal_ctn.children[0].children[0].children[3].classList.add("d-none");
    });

    modal_ctn.children[0].children[0].children[1].innerHTML = "Steambox - " + steam["label"];
    modal_ctn.children[0].children[0].children[2].children[0].children[0].children[0].addEventListener(
        "click",
        function () {
            modal_ctn.children[0].children[0].children[2].classList.remove("d-block");
            modal_ctn.children[0].children[0].children[2].classList.add("d-none");

            modal_ctn.children[0].children[0].children[3].classList.remove("d-none");
            modal_ctn.children[0].children[0].children[3].classList.add("d-block");

            //clearTimeout(main_timer);
        },
        false
    );
    modal_ctn.children[0].children[0].children[3].children[1].children[0].children[0].addEventListener(
        "click",
        function () {
            $.ajax({
                type: "POST", //type of method
                url: "http://localhost/backend/postdm.php", //your page
                data: { action_id: 9, task_id: task["id"] }, // passing the values
                success: function (res) {
                    document.getElementById("a-home").click();
                    $("#modal-task-id-" + task["id"]).modal("hide");
                },
                fail: function (onfail) {
                    document.getElementById("a-home").click();
                },
            });
        },
        false
    );
    modal_ctn.children[0].children[0].children[2].children[0].children[1].children[0].addEventListener(
        "click",
        function () {
            modal_ctn.children[0].children[0].children[2].classList.remove("d-block");
            modal_ctn.children[0].children[0].children[2].classList.add("d-none");

            modal_ctn.children[0].children[0].children[4].classList.remove("d-none");
            modal_ctn.children[0].children[0].children[4].classList.add("d-block");

            //clearTimeout(main_timer);
        },
        false
    );
    modal_ctn.children[0].children[0].children[4].children[0].children[2].addEventListener(
        "click",
        function () {
            "use strict";

            var forms = document.querySelectorAll(".needs-validation");

            Array.prototype.slice.call(forms).forEach(function (form) {
                form.addEventListener(
                    "submit",
                    function (event) {
                        if (!form.checkValidity()) {
                            event.preventDefault();
                            event.stopPropagation();
                        } else {
                            //0 0 4 0 1 0
                            const add_time_value = modal_ctn.children[0].children[0].children[4].children[0].children[1].children[0].value;
                            $.ajax({
                                type: "POST", //type of method
                                url: "http://localhost/backend/postdm.php", //your page
                                data: { action_id: 10, time_value: add_time_value, task_id: task["id"] }, // passing the values
                                success: function (res) {
                                    document.getElementById("a-home").click();
                                    $("#modal-task-id-" + task["id"]).modal("hide");
                                },
                                fail: function (onfail) {
                                    home();
                                },
                            });
                            //submitTask(id);
                        }

                        event.preventDefault();

                        form.classList.remove("needs-validation");
                        form.classList.add("was-validated");
                    },
                    false
                );
            });
        },
        false
    );

    if (items.length == 1) {
        const str = lots[0] + " / " + colors[0] + " / " + items[0] + " Gld";
        item_ctn.children[1].innerHTML = str;
    } else {
        const str = lots[0] + " / " + colors[0] + " / " + items[0] + " Gld";
        item_ctn.children[1].innerHTML = str;

        for (i = 1; i < items.length - 1; i++) {
            const child = item_ctn.cloneNode(true);

            const str = lots[i] + " / " + colors[i] + " / " + items[i] + " Gld";

            child.children[0].innerHTML = "Item " + (i + 1) + ":";
            child.children[1].innerHTML = str;

            parent_item_ctn.appendChild(child);
        }
    }
}

function assignResepToList() {
    const list = document.getElementById("dataListResep");
    list.innerHTML = "";

    console.log(list);
    map_resep.forEach(function (element) {
        const option = document.createElement("option");
        option.value = element["nama"];

        list.appendChild(option);
    });
}

function assignUserToTable() {
    const table = document.getElementById("user-table-body");

    var index = 0;

    map_user.forEach(function (element) {
        const row = table.insertRow(index);
        const durasi_int = parseInt(element["durasi"]);

        var durasi_str = "";

        /*
        if (durasi_int % 60 == 0) {
            durasi_str = durasi_int / 60 + "jam";
        } else {
            durasi_str = durasi_int / 60 + "jam " + (durasi_int % 60) + "menit";
        }
        */

        const cell_id = row.insertCell(0);
        const cell_username = row.insertCell(1);
        const cell_password = row.insertCell(2);
        const cell_role = row.insertCell(3);

        cell_id.innerHTML = element["id"];
        cell_username.innerHTML = element["username"];
        cell_password.innerHTML = element["password"];
        cell_role.innerHTML = element["role"];

        index++;
    });
}

function assignColorToList() {
    const list = document.getElementById("dataListColor");
    list.innerHTML = "";

    map_color.forEach(function (element) {
        const option = document.createElement("option");
        option.value = element["nama"];

        list.appendChild(option);
    });
}

function assignColorToTable() {
    const table = document.getElementById("color-table-body");

    var index = 0;

    map_color.forEach(function (element) {
        const row = table.insertRow(index);

        const cell_id = row.insertCell(0);
        const cell_nama = row.insertCell(1);

        cell_id.innerHTML = element["id"];
        cell_nama.innerHTML = element["nama"];
        index++;
    });
}
