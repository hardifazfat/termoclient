function assignTask(id) {
    console.log(id);
    const isAssign = document.getElementById("mesin-tab-assign-" + id).classList.contains("d-block");

    if (!isAssign) {
        document.getElementById("mesin-tab-idle-" + id).classList.remove("d-block");
        document.getElementById("mesin-tab-idle-" + id).classList.add("d-none");
        document.getElementById("mesin-tab-assign-" + id).classList.remove("d-none");
        document.getElementById("mesin-tab-assign-" + id).classList.add("d-block");

        //addAssignItem(id, 0);
    } else {
        document.getElementById("mesin-tab-idle-" + id).classList.remove("d-none");
        document.getElementById("mesin-tab-idle-" + id).classList.add("d-block");
        document.getElementById("mesin-tab-assign-" + id).classList.remove("d-block");
        document.getElementById("mesin-tab-assign-" + id).classList.add("d-none");
        document.getElementById("resep-val-" + id).value = "";
        document.getElementById("select-troli").value = "default";
        document.getElementById("additional-val-" + id).value = "";
        document.getElementById("check-nmi-" + id).classList.remove("d-block");
        document.getElementById("check-nmi-" + id).classList.add("d-none");
        document.getElementById("troli-ctn-" + id).classList.remove("d-block");
        document.getElementById("troli-ctn-" + id).classList.add("d-none");
        document.getElementById("additional-ctn-" + id).classList.remove("d-block");
        document.getElementById("additional-ctn-" + id).classList.add("d-none");
        document.getElementById("btn-terapkan-" + id).classList.remove("d-block");
        document.getElementById("btn-terapkan-" + id).classList.add("d-none");
        document.getElementById("presetqty-" + id).value = "default";

        const parentCtn = document.getElementById("detail-barang-ctn-" + id);
        parentCtn.innerHTML = "";
    }

    assignResepToList();
    assignColorToList();
}

function addAssignItem(id) {
    document.getElementById("barang-no").classList.remove("d-none");
    document.getElementById("barang-no").classList.add("d-block");

    var element_count = document.getElementById("detail-barang-ctn-" + id).childElementCount;

    jumlah_assign_barang += 1;

    var jumlah_assigned = 0;

    for (var i = 0; i < element_count; i++) {
        const jml = document.getElementById("assign-item-" + id + (i + 1)).children[3].children[1].value;
        jumlah_assigned += parseInt(jml);
    }

    const assign_id = "assign-item-" + id + (element_count + 1);
    var batchCtn = document.getElementById("detail-barang").cloneNode(true);
    batchCtn.classList.remove("d-none");
    batchCtn.id = assign_id;

    const jumlah_in = batchCtn.children[3].children[1];
    console.log("moooooiiiiiooo");
    if (max_qty_allowed != 1000000) {
        console.log("moooooooooooooooooooooooo");
        jumlah_in.value = max_qty_allowed - jumlah_assigned;

        jumlah_in.addEventListener(
            "change",
            function () {
                const assign_len = document.getElementById("detail-barang-ctn-" + id).childElementCount;
                const id_index = assign_id.split("assign-item-" + id)[1];

                jumlah_assigned = 0;

                for (var i = 0; i < assign_len; i++) {
                    const jml = document.getElementById("assign-item-" + id + (i + 1)).children[3].children[1].value;
                    jumlah_assigned += parseInt(jml);
                }

                if (assign_len === parseInt(id_index)) {
                    if (jumlah_assigned < max_qty_allowed) {
                        addAssignItem(id);
                    }
                }
            },
            false
        );
    }

    batchCtn.children[2].children[1].addEventListener(
        "change",
        function () {
            const resep_name = document.getElementById("resep-val-" + id).value;
            const resep = getResepByName(resep_name);
            const warna = batchCtn.children[1].children[1].value;
            const batch_no = batchCtn.children[2].children[1].value;
            const task_already = isBatchAlready(batch_no, warna);
            const parentCtn = document.getElementById("detail-barang-ctn-" + id);
            const len = parentCtn.childElementCount;
            const resep_bq = resep["per_batch"];

            var isAlready = false;

            for (i = 0; i < len - 1; i++) {
                if (null != parentCtn.children[i]) {
                    if (null != parentCtn.children[i].id) {
                        const batch = parentCtn.children[i].children[2].children[1].value;
                        const warna_ = parentCtn.children[i].children[1].children[1].value;

                        if (parseInt(batch_no) == parseInt(batch) && warna.localeCompare(warna_) == 0) {
                            isAlready = true;
                        }
                    }
                }
            }

            if (isAlready) {
                batchCtn.children[2].children[1].value = "Batch Invalid";
                alert("Warna " + warna + " Nomor batch " + batch_no + " sudah diisi pada item sebelumnya");
            } else {
                if (null != task_already) {
                    const recent_qty = parseInt(getQTYByBatch(task_already, batch_no));
                    var str = " Resep " + getNamaResepByID(task_already["resep_id"]) + " " + warna + " Batch no " + batch_no + " telah dikerjakan pada hari ini di Steambox - " + getSteamByID(task_already["mesin_id"])["label"] + " dengan qty " + recent_qty + " Gelondong <br/><br/>";

                    if (recent_qty < resep_bq) {
                        str = str + "Apakah anda ingin <b>melanjutkan sisa batch 1 : " + (resep_bq - recent_qty) + " Gld ?</b>";
                        $("#pr-pyn-ok").click(function () {
                            batchCtn.children[3].children[1].value = resep_bq - recent_qty;
                            batchCtn.children[3].children[1].dispatchEvent(new Event("change"));
                            $("#modal-pr-pyn").modal("hide");
                        });
                    } else {
                        str = str + "Apakah anda tetap ingin melanjutkan ?";
                        $("#pr-pyn-ok").click(function () {
                            $("#modal-pr-pyn").modal("hide");
                        });
                    }

                    $("#pr-pyn-cancel").click(function () {
                        $("#modal-pr-pyn").modal("hide");
                    });

                    $("#modal-pr-pyn").modal("show");
                    $("#pr-pyn-msg").html(str);
                }
            }
        },
        false
    );

    document.getElementById("detail-barang-ctn-" + id).appendChild(batchCtn);
    const textNo = document.getElementById(assign_id).children[0];

    textNo.classList.remove("d-none");
    textNo.classList.add("d-block");
    textNo.innerHTML = "Item-" + (element_count + 1);

    if (max_qty_allowed == 1000000) {
    } else if (jumlah_assigned < max_qty_allowed) {
        const delItem = document.getElementById(assign_id).children[4];

        if (element_count + 1 > 1) {
            delItem.classList.remove("d-none");
            delItem.classList.add("d-block");
        }

        delItem.id = "delete-assign-id-" + id + (element_count + 1);
        delItem.addEventListener(
            "click",
            function () {
                delAssignItem(this.id, id);
            },
            false
        );
    } else {
        jumlah_assigned = 0;

        for (var i = 0; i < element_count - 1; i++) {
            const jml = document.getElementById("assign-item-" + id + (i + 1)).children[3].children[1].value;
            jumlah_assigned += parseInt(jml);
        }

        const jumlah_in = document.getElementById("assign-item-" + id + element_count).children[3].children[1];

        if (max_qty_allowed > jumlah_assigned) {
            jumlah_in.value = max_qty_allowed - jumlah_assigned;
        } else {
            jumlah_in.value = 0;
        }
    }
}

function delAssignItem(id_to_split, id) {
    const parentCtn = document.getElementById("detail-barang-ctn-" + id);
    const child = parentCtn.childElementCount;

    var i = 0;

    const idInt = parseInt(id_to_split.split("delete-assign-id-" + id)[1]);

    const id_to_del = "assign-item-" + id + idInt;

    console.log(id + id_to_del);
    console.log(parentCtn.childNodes);

    parentCtn.removeChild(document.getElementById(id_to_del));

    for (i = 0; i < child; i++) {
        if (null != parentCtn.children[i]) {
            parentCtn.children[i].id = "assign-item-" + id + (i + 1);

            const textNo = parentCtn.children[i].children[0];
            const delItem = parentCtn.children[i].children[4];

            textNo.innerHTML = "Item-" + (i + 1);
            delItem.id = "delete-assign-id-" + id + (i + 1);
        }
    }

    /*
     var jumlah_assigned = 0;
 
     for (i = 0; i < child - 2; i++) {
         const jml = document.getElementById("assign-item-" + id + (i + 1)).children[2].children[1].value;
         jumlah_assigned += parseInt(jml);
     }
 
     const jumlah_in = document.getElementById("assign-item-" + id + (child - 1)).children[2].children[1];
     jumlah_in.value = max_qty_allowed - jumlah_assigned;
     */

    /*
     parentCtn.childNodes.forEach(function (list) {
         console.log(list);
         if (null != parentCtn.children[i]) {
             const textNo = parentCtn.children[i].children[0];
             const delItem = parentCtn.children[i].children[4];
 
             textNo.innerHTML = "Item-" + (i + 1);
             delItem.id = "delete-assign-id" + (i + 1);
         }
 
         i++;
     });
     */
}

function submitTask(id) {
    const resep = document.getElementById("resep-val-" + id).value;
    const presetqty_val = document.getElementById("presetqty-" + id).value;
    const additional = document.getElementById("additional-val-" + id).value;
    const pic = document.getElementById("pic-val").value;
    const parentCtn = document.getElementById("detail-barang-ctn-" + id);
    const mesin_id = document.getElementById("steam-id-" + id).value;
    const keterangan = document.getElementById("presetket-" + id).value;

    var lots = "";
    var jml = "";
    var color = "";

    var i = 0;

    const recent_additional = parseInt(getAdditional(resep));

    parentCtn.childNodes.forEach(function (nodes) {
        if (null != parentCtn.children[i]) {
            if (null != parentCtn.children[i].id) {
                const batch = parentCtn.children[i].children[2].children[1].value;
                const jumlah = parentCtn.children[i].children[3].children[1].value;
                const warna = parentCtn.children[i].children[1].children[1].value;
                //const warna = parentCtn.children[i].children[3].children[1].options[warna_val].text;

                lots += batch + ",,,";
                jml += jumlah + ",,,";
                color += warna + ",,,";
            }
        }
        i++;
    });

    if (recent_additional == parseInt(additional)) {
        $.ajax({
            type: "POST", //type of method
            url: "http://localhost/backend/postdm.php", //your page
            data: { action_id: 3, resep_id: resep, mesin_id: mesin_id, items: jml, lots: lots, pic: pic, additional: additional, colors: color, troli: presetqty_val, ket: keterangan }, // passing the values
            success: function (res) {
                console.log("Asdasd");
                document.getElementById("a-home").click();
                $("#modalPSQ").modal("hide");
            },
            fail: function (res) {
                console.log("asd");
            },
        });
    } else {
        $("#pr-pyn-cancel").click(function () {
            $("#modal-pr-pyn").modal("hide");
        });

        $("#pr-pyn-ok").click(function () {
            $.ajax({
                type: "POST", //type of method
                url: "http://localhost/backend/postdm.php", //your page
                data: { action_id: 3, resep_id: resep, mesin_id: mesin_id, items: jml, lots: lots, pic: pic, additional: additional, colors: color, troli: presetqty_val, ket: keterangan }, // passing the values
                success: function (res) {
                    console.log("Asdasd");
                    document.getElementById("a-home").click();
                    $("#modalPSQ").modal("hide");
                    $("#modal-pr-pyn").modal("hide");
                },
                fail: function (res) {
                    console.log("asd");
                },
            });
        });

        $("#modal-pr-pyn").modal("show");
        $("#pr-pyn-msg").html("Nilai tambahan waktu anda tidak sama dengan pemasakan " + resep + " sebelumnya<br/><br/>" + "Apakah anda tetap ingin melanjutkan ?");
    }
}

function validate(id, bypass) {
    const parentCtn = document.getElementById("detail-barang-ctn-" + id);
    const child = parentCtn.childElementCount;
    const form = document.getElementById("forms-idle-" + id);

    var jumlah_assigned = 0;
    console.log("adasdasdas" + child);
    for (i = 0; i < child; i++) {
        const jml = document.getElementById("assign-item-" + id + (i + 1)).children[3].children[1].value;
        jumlah_assigned += parseInt(jml);
    }

    if (form.checkValidity()) {
        if (max_qty_allowed === 1000000 || bypass) {
            submitTask(id);
        } else if (!bypass) {
            if (jumlah_assigned < max_qty_allowed) {
                $("#modalPSQ").modal("show");
            } else if (jumlah_assigned > max_qty_allowed) {
                $("#pr-py-ok").click(function () {
                    $("#modal-pr-py").modal("hide");
                });

                $("#modal-pr-py").modal("show");
                $("#pr-py-msg").html("Jumlah gelondong melebihi kapasistas troli");
            } else if (jumlah_assigned == max_qty_allowed) {
                submitTask(id);
            }
        }
    }

    form.classList.remove("needs-validation");
    form.classList.add("was-validated");
}

function isBatchAlready(txt, color) {
    var last_task = null;

    console.log(map_task_today);

    map_task_today.forEach(function (element) {
        const lot = element["lots"].split(",,,");
        const color_ = element["colors"].split(",,,");

        var index = 0;

        lot.forEach(function (key) {
            console.log(key + " " + txt);
            if (parseInt(key) == parseInt(txt) && color_[index].localeCompare(color) == 0) {
                last_task = element;
            }

            index++;
        });
    });

    console.log(last_task);

    return last_task;
}

function getQTYByBatch(task, batch) {
    const qty = task["items"].split(",,,");
    const lots = task["lots"].split(",,,");

    var ret;

    for (i = 0; i < qty.length; i++) {
        if (batch.localeCompare(lots[i]) == 0) {
            ret = qty[i];
        }
    }

    return ret;
}
