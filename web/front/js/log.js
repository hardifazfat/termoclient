var map_filter_steam = [];
var map_filter_resep = [];
var map_filter_date = [];

if (null == map_history) {
    $("#log-ctn").hide();
    $("#txt-log-none").show();
} else {
    if (map_history.length > 0) {
        $("#log-ctn").show();
        $("#txt-log-none").hide();
    } else {
        $("#log-ctn").hide();
        $("#txt-log-none").show();
    }
}

function validateResep() {
    map_filter_steam = [];
    map_filter_resep = [];
    var map_date = [];
    var map_sb = [];
    var map_final = [];

    var parent_child;
    const parent_steam = document.getElementById("log-sb-list");
    const parent_resep = document.getElementById("log-r-list");
    const filter_date = document.getElementById("daterange-in").value;
    const is_interval_checked = document.getElementById("filter-ctn").children[0].children[0].children[0].checked;
    parent_child = parent_steam.childElementCount;

    console.log(is_interval_checked);

    for (i = 0; i < parent_child; i++) {
        const child = parent_steam.children[i];

        if (child.children[0].checked) {
            map_filter_steam.push(child.children[1].innerHTML.split("Steambox - ")[1]);
        }
    }

    parent_child = parent_resep.childElementCount;

    for (i = 0; i < parent_child; i++) {
        const child = parent_resep.children[i];

        if (child.children[0].checked) {
            map_filter_resep.push(child.children[1].innerHTML);
        }
    }

    if (filter_date.localeCompare("") == 0 || !is_interval_checked) {
        map_date = map_history;
    } else {
        map_filter_date = generateFilterDate(filter_date);

        map_history.forEach(function (element) {
            const history_date = getTimeStr(true, element["heating_timestamp"]).split(" ")[0];

            map_filter_date.forEach(function (date) {
                if (date.localeCompare(history_date) == 0) {
                    map_date.push(element);
                }
            });
        });
    }

    map_date.forEach(function (element) {
        const task = getTaskByID(element["task_id"]);
        const steam = getSteamByID(task["mesin_id"]);
        const label = steam["label"];

        map_filter_steam.forEach(function (sb) {
            if (sb.localeCompare(label) == 0) {
                map_sb.push(element);
            }
        });
    });

    if (map_filter_steam.length == 0) {
        map_sb = map_date;
    }

    map_sb.forEach(function (element) {
        const task = getTaskByID(element["task_id"]);
        const nama = getNamaResepByID(task["resep_id"]);

        map_filter_resep.forEach(function (res) {
            if (res.localeCompare(nama) == 0) {
                map_final.push(element);
            }
        });
    });

    if (map_filter_resep.length == 0) {
        map_final = map_sb;
    }

    document.getElementById("filter-ctn").classList.remove("d-flex");
    document.getElementById("filter-ctn").classList.add("d-none");
    assignLogToTable(map_final);
}

function generateFilterDate(str) {
    var map_ret = [];
    const str_date_start = str.split("-")[0];
    const str_date_end = str.split("-")[1];

    var date_split = str_date_start.split("/");
    const date_start = new Date(date_split[2], parseInt(date_split[0]) - 1, date_split[1]);
    date_split = str_date_end.split("/");
    const date_end = new Date(date_split[2], parseInt(date_split[0]) - 1, date_split[1]);

    const date_start_milis = date_start.getTime();
    const date_end_milis = date_end.getTime();

    for (i = date_start_milis; i < date_end_milis + 86400000; i += 86400000) {
        const date_formatted = new Date(i).format("dd/mm/yyyy");
        map_ret.push(date_formatted);
    }

    return map_ret;
}

function generateXLS() {
    const fileName = "Logs" + new Date().format("ddmmyyyyHHMMss") + ".xlsx";
    const parent_ctn = document.getElementById("table-log");
    const pc_count = parent_ctn.childElementCount;

    var wb = XLSX.utils.book_new();
    wb.Props = {
        Title: "Log data steambox",
        Subject: "Test",
        Author: "Hardianto Dwipa",
        CreatedDate: new Date(),
    };

    wb.SheetNames.push("Log");
    var ws;

    var data = ["No", "Tanggal", "Steambox", "Nama Resep", "Troli", "Batch", "QTY", "Total Waktu Tambahan (menit)", "Jam pemanasan", "Durasi Pemanasan", "Jam Pemasakan", "Durasi Pemasakan", "PIC", "Keterangan"];
    ws = XLSX.utils.sheet_add_aoa(ws, [data], { origin: -1 });

    var i = 1;
    map_history_current.forEach(function (element) {
        data = [];
        const task = getTaskByID(element["task_id"]);
        const steam = getSteamByID(task["mesin_id"]);
        const heating_time = element["heating_timestamp"];
        const cooking_time = element["cooking_timestamp"];
        const finish_time = element["finish_timestamp"];

        const batch_split = task["lots"].split(",,,");
        const qty_split = task["items"].split(",,,");

        const pemanasan = cooking_time == 0 ? "-" : getDuration(heating_time, cooking_time);
        const pemasakan = cooking_time == 0 ? "-" : finish_time == 0 ? "-" : getTimeStr(true, cooking_time);
        const durasi_masak = finish_time == 0 ? "-" : getDuration(cooking_time, finish_time);
        var add_time = 0;

        task["additional"].split(",,,").forEach(function (split) {
            const int_val = parseInt(split);
            add_time += int_val;
        });

        data.push(i);
        data.push(getTimeStr(true, element["heating_timestamp"]).split(" ")[0]);
        data.push("Steambox - " + steam["label"]);
        data.push(getNamaResepByID(task["resep_id"]));
        data.push(task["troli"]);
        data.push(batch_split[0]);
        data.push(qty_split[0]);

        if (batch_split.length > 1) {
            for (index = 1; index < batch_split.length - 1; index++) {
                var data_ = [];
                data_.push("");
                data_.push("");
                data_.push("");
                data_.push("");
                data_.push("");
                data_.push(batch_split[index]);
                data_.push(qty_split[index]);

                ws = XLSX.utils.sheet_add_aoa(ws, [data_], { origin: -1 });
            }
        }

        data.push(add_time);
        data.push(getTimeStr(true, heating_time));
        data.push(pemanasan);
        data.push(pemasakan);
        data.push(durasi_masak);
        data.push(task["pic"]);
        data.push(element["remark"]);

        ws = XLSX.utils.sheet_add_aoa(ws, [data], { origin: -1 });

        i++;
    });

    wb.Sheets["Log"] = ws;

    var wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });
    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff;
        return buf;
    }
    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), fileName);
}
