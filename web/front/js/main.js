var user_name = "";
var user_role;
var main_timer = "";
var is_timer_stop = false;

function resep() {
    is_timer_stop = true;
    $("#content").load("resep.html");
    console.log("resep clicked!!");
}
function home() {
    $("#content").load("home.html");
    is_timer_stop = false;
    clearTimeout(main_timer);
    //getMesinData();
    //main_timer = setTimeout(getMesinData, 1000);
    console.log("home clicked!!");
}
function log() {
    is_timer_stop = true;
    clearTimeout(main_timer);
    $("#content").load("log.html");
    console.log("log clicked!!");
}
function daftarAkun() {
    is_timer_stop = true;
    clearTimeout(main_timer);
    $("#content").load("daftarakun.html");
    console.log("Daftar Akun clicked!!");
}
function warna() {
    is_timer_stop = true;
    clearTimeout(main_timer);
    $("#content").load("warna.html");
    console.log("Warna clicked!!");
}

home();

var jumlah_assign_barang = 1;

function displayTypeOperator() {
    console.log("MODE OPERATOR");
    document.getElementById("sidebarMenu").children[0].children[0].children[1].classList.add("d-none");
    document.getElementById("sidebarMenu").children[0].children[0].children[2].classList.add("d-none");
    document.getElementById("sidebarMenu").children[0].children[0].children[3].classList.add("d-none");
}
function displayTypeAdmin() {
    document.getElementById("sidebarMenu").children[0].children[0].children[2].classList.add("d-none");
}
function applyUSERNAME(name) {
    user_name = name;
}
function applyPICName() {
    document.getElementById("pic-val").value = user_name;
}

function mesinDetail() {
    console.log("eye clicked!");
    const isExpand = document.getElementById("mesin-detail-tab").classList.contains("d-flex");

    if (!isExpand) {
        document.getElementById("mesin-eye").classList.remove("eye-icon-d");
        document.getElementById("mesin-eye").classList.add("eye-icon-select");
        document.getElementById("mesin-tab").classList.remove("col-4");
        document.getElementById("mesin-tab").classList.add("col-7");
        document.getElementById("mesin-main-tab").classList.remove("w-100");
        document.getElementById("mesin-main-tab").classList.add("w-50");
        document.getElementById("mesin-detail-tab").classList.remove("d-none");
        document.getElementById("mesin-detail-tab").classList.add("d-flex");
    } else {
        document.getElementById("mesin-eye").classList.remove("eye-icon-select");
        document.getElementById("mesin-eye").classList.add("eye-icon-d");
        document.getElementById("mesin-tab").classList.remove("col-7");
        document.getElementById("mesin-tab").classList.add("col-4");
        document.getElementById("mesin-main-tab").classList.remove("w-50");
        document.getElementById("mesin-main-tab").classList.add("w-100");
        document.getElementById("mesin-detail-tab").classList.remove("d-flex");
        document.getElementById("mesin-detail-tab").classList.add("d-none");
    }
}

function validateAkun() {
    "use strict";

    var forms = document.querySelectorAll(".needs-validation");

    Array.prototype.slice.call(forms).forEach(function (form) {
        form.addEventListener(
            "submit",
            function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                } else {
                    submitAkun();
                }

                form.classList.remove("needs-validation");
                form.classList.add("was-validated");
            },
            false
        );
    });
}

function submitAkun() {
    const role_val = document.getElementById("role-val").value;
    const role = document.getElementById("role-val").options[role_val].text;
    const username = document.getElementById("user-nama").value;
    const password = document.getElementById("user-password").value;

    console.log("sadasdsa");
    $.ajax({
        type: "POST", //type of method
        url: "http://localhost/backend/postdm.php", //your page
        data: { action_id: 7, username: username, role: role, password: password }, // passing the values
        success: function (res) {
            //do what you want here...
        },
    });
}

function validateColor() {
    "use strict";

    var forms = document.querySelectorAll(".needs-validation");

    Array.prototype.slice.call(forms).forEach(function (form) {
        form.addEventListener(
            "submit",
            function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                } else {
                    submitColor();
                }

                form.classList.remove("needs-validation");
                form.classList.add("was-validated");
            },
            false
        );
    });
}

function submitColor() {
    const nama = document.getElementById("color-nama").value;

    $.ajax({
        type: "POST", //type of method
        url: "http://localhost/backend/postdm.php", //your page
        data: { action_id: 8, nama: nama }, // passing the values
        success: function (res) {
            //do what you want here...
        },
    });
}
function estimateEnd(isCountDown, dateStart, duration) {
    //20220304234905
    const duration_milis = duration * 60 * 1000;
    const year1 = dateStart.slice(0, 4);
    const month1 = dateStart.slice(4, 6);
    const date_1 = dateStart.slice(6, 8);
    const hour1 = dateStart.slice(8, 10);
    const minute1 = dateStart.slice(10, 12);
    const sec1 = dateStart.slice(12, 14);

    const d1 = new Date(year1, parseInt(month1) - 1, date_1, hour1, minute1, sec1);

    var time_end = new Date(d1.getTime() + duration_milis);
    var ms, secs, mins, hrs;

    if (isCountDown) {
        time_end = new Date(time_end.getTime() - Date.now()).getTime();

        ms = time_end % 1000;
        time_end = (time_end - ms) / 1000;
        secs = time_end % 60;
        time_end = (time_end - secs) / 60;
        mins = time_end % 60;
        hrs = (time_end - mins) / 60;
    } else {
        hrs = time_end.getHours();
        mins = time_end.getMinutes();
        secs = time_end.getSeconds();
    }

    if (hrs < 10) {
        hrs = "0" + hrs;
    }
    if (mins < 10) {
        mins = "0" + mins;
    }
    if (secs < 10) {
        secs = "0" + secs;
    }

    return hrs + ":" + mins + ":" + secs;
}

function isAboveLimit(time, std) {
    const time_split = time.split(":");
    const time_sec = parseInt(time_split[0]) * 3600 + parseInt(time_split[1]) * 60 + parseInt(time_split[2]) * 1;
    const std_sec = std * 60;

    return time_sec > std_sec;
}
function isBelowLimit(time, std) {
    const time_split = time.split(":");
    const time_sec = parseInt(time_split[0]) * 3600 + parseInt(time_split[1]) * 60 + parseInt(time_split[2]) * 1;
    const std_sec = std * 60;

    return time_sec < std_sec;
}

function getDuration(date1, date2) {
    //val format : 20220215163816 yyyyMMddhhmmss
    /*
    const date1 = new Date('7/13/2010');
    const date2 = new Date('12/15/2010');
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
    console.log(diffTime + " milliseconds");
    console.log(diffDays + " days");
    */

    const year1 = date1.slice(0, 4);
    const year2 = date2.slice(0, 4);
    const month1 = date1.slice(4, 6);
    const month2 = date2.slice(4, 6);
    const date_1 = date1.slice(6, 8);
    const date_2 = date2.slice(6, 8);
    const hour1 = date1.slice(8, 10);
    const hour2 = date2.slice(8, 10);
    const minute1 = date1.slice(10, 12);
    const minute2 = date2.slice(10, 12);
    const sec1 = date1.slice(12, 14);
    const sec2 = date2.slice(12, 14);

    const d1 = new Date(year1, parseInt(month1) - 1, date_1, hour1, minute1, sec1);
    const d2 = new Date(year2, parseInt(month2) - 1, date_2, hour2, minute2, sec2);

    var diff_milis = Math.abs(d1 - d2);

    const ms = diff_milis % 1000;
    diff_milis = (diff_milis - ms) / 1000;
    var secs = diff_milis % 60;
    diff_milis = (diff_milis - secs) / 60;
    var mins = diff_milis % 60;
    var hrs = (diff_milis - mins) / 60;

    if (hrs < 10) {
        hrs = "0" + hrs;
    }
    if (mins < 10) {
        mins = "0" + mins;
    }
    if (secs < 10) {
        secs = "0" + secs;
    }

    return hrs + ":" + mins + ":" + secs;
}

function getTimeStr(isFull, val) {
    //val format : 20220215163816 yyyyMMddhhmmss
    var str_rt;
    const val_str = val.toString();
    const year = val_str.slice(0, 4);
    const month = val_str.slice(4, 6);
    const day = val_str.slice(6, 8);
    const hour = val_str.slice(8, 10);
    const minute = val_str.slice(10, 12);
    const second = val_str.slice(12, 14);

    if (!isFull) {
        str_rt = hour + ":" + minute + ":" + second;
    } else {
        str_rt = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
    }

    return str_rt;
}

function isTimeOut(val) {
    var timeout = false;

    const time = getTimeStr(false, val);
    const time_tc = new Date();
    const time_split = time.split(":");
    const jam = parseInt(time_split[0]);
    const jam_tc = parseInt(time_tc.format("HH"));
    const min = parseInt(time_split[1]);
    const min_tc = parseInt(time_tc.format("MM"));
    const det = parseInt(time_split[2]);
    const det_tc = parseInt(time_tc.format("ss"));

    if (jam == jam_tc) {
        if (min == min_tc) {
            if (Math.abs(det - det_tc) > 10) {
                timeout = true;
            }
        } else {
            timeout = true;
        }
    } else {
        timeout = true;
    }

    return timeout;
}
