if (null == map_resep) {
   $("#resep-table").hide();
   $("#txt-resep-none").show();
} else {
   if (map_resep.length > 0) {
      $("#resep-table").show();
      $("#txt-resep-none").hide();
   } else {
      $("#resep-table").hide();
      $("#txt-resep-none").show();
   }
}
function assignResepToTable(map) {
   const table = document.getElementById("resep-table-body");
   table.innerHTML = "";
   var index = 0;

   map.forEach(function (element) {
      const row = table.insertRow(index);

      var btn_edit = document.createElement("button");
      btn_edit.className = "btn btn-secondary btn-sm px-2";
      btn_edit.innerText = "Detail";
      btn_edit.addEventListener(
         "click",
         function () {
            document.getElementById("resep-detail-parent").children[0].className = "";
            document.getElementById("resep-nama").innerHTML = element["nama"];
            document.getElementById("resep-nama-input").value = element["nama"];
            document.getElementById("resep-durasi").innerHTML = element["durasi"] + " menit";
            document.getElementById("resep-durasi-input").value = element["durasi"];
            document.getElementById("resep-id").value = element["id"];
            document.getElementById("troli-satu").innerHTML = element["troli_satu"] + " gld";
            document.getElementById("troli-satu-input").value = element["troli_satu"];
            document.getElementById("troli-dua").innerHTML = element["troli_dua"] + " gld";
            document.getElementById("troli-dua-input").value = element["troli_dua"];
            document.getElementById("troli-tiga").innerHTML = element["troli_tiga"] + " gld";
            document.getElementById("troli-tiga-input").value = element["troli_tiga"];
            document.getElementById("std-time-max").innerHTML = element["std_cook_max"] + " menit";
            document.getElementById("std-time-max-input").value = element["std_cook_max"];
            document.getElementById("std-time-min").innerHTML = element["std_cook_min"] + " menit";
            document.getElementById("std-time-min-input").value = element["std_cook_min"];
            document.getElementById("staticBackdropLabel").innerHTML = "Data resep";
            //
            document.getElementById("resep-detail-parent").classList.remove("d-none");
            document.getElementById("resep-detail-parent").classList.add("d-block");

            console.log(document.getElementById("resep-detail-parent").children[0]);
            document.getElementById("resep-detail-parent").children[0].className = "animate__animated animate__slideInRight";
            console.log(document.getElementById("resep-detail-parent").children[0]);

            document.getElementById("resep-detail-parent").children[0].addEventListener("animationend", () => {
               document.getElementById("resep-detail-parent").children[0].className = "";
               console.log(document.getElementById("resep-detail-parent").children[0]);
            });
         },
         false
      );

      const cell_no = row.insertCell(0);
      const cell_nama = row.insertCell(1);
      const cell_durasi = row.insertCell(2);
      const cell_edit = row.insertCell(3);

      cell_no.innerHTML = element["id"];
      cell_nama.innerHTML = element["nama"];
      cell_durasi.innerHTML = element["durasi"] + " menit";
      cell_edit.appendChild(btn_edit);

      index++;
   });
}
function validateResep() {
   "use strict";

   var forms = document.querySelectorAll(".needs-validation");

   Array.prototype.slice.call(forms).forEach(function (form) {
      form.addEventListener(
         "submit",
         function (event) {
            if (!form.checkValidity()) {
               event.preventDefault();
               event.stopPropagation();
            } else {
               submitResep();
            }

            form.classList.remove("needs-validation");
            form.classList.add("was-validated");
            event.preventDefault();
         },
         false
      );
   });
}

function submitResep() {
   const resep_nama = document.getElementById("resep-nama-input").value;
   const resep_durasi = parseInt(document.getElementById("resep-durasi-input").value);
   const resep_id = document.getElementById("resep-id").value;
   const troli_satu = document.getElementById("troli-satu-input").value;
   const troli_dua = document.getElementById("troli-dua-input").value;
   const troli_tiga = document.getElementById("troli-tiga-input").value;
   const std_time_min = parseInt(document.getElementById("std-time-min-input").value);
   const std_time_max = parseInt(document.getElementById("std-time-max-input").value);
   const qpb = document.getElementById("resep-qpb-input").value;

   const err_ctn = document.getElementById("std-time-err");

   if (std_time_min >= std_time_max) {
      err_ctn.classList.remove("d-none");
      err_ctn.classList.add("d-block");

      err_ctn.innerHTML = "Durasi minimum tidak boleh lebih besar atau sama dengan nilai maksimum";
   } else if (std_time_max < resep_durasi) {
      err_ctn.classList.remove("d-none");
      err_ctn.classList.add("d-block");

      err_ctn.innerHTML = "Durasi masak maximum tidak boleh lebih kecil dari nilai standar";
   } else {
      err_ctn.classList.remove("d-block");
      err_ctn.classList.add("d-none");

      $.ajax({
         type: "POST", //type of method
         url: "http://localhost/backend/postdm.php", //your page
         data: { action_id: 6, resep_id: resep_id, resep_nama: resep_nama, resep_durasi: resep_durasi, troli_satu: troli_satu, troli_dua: troli_dua, troli_tiga: troli_tiga, std_time_min: std_time_min, std_time_max: std_time_max, qpb: qpb }, // passing the values
         success: function (res) {
            document.getElementById("a-home").click();
            $("#modal-edit-resep").modal("hide");
         },
         fail: function (onfail) {
            console.log("asdiajdsadisaifsssssssssssssssssssssssa");
            home();
         },
      });
   }
}

function deleteResep() {
   const id = document.getElementById("resep-id").value;
   $.ajax({
      type: "POST", //type of method
      url: "http://localhost/backend/postdm.php", //your page
      data: { action_id: 6, resep_id: id, resep_nama: "", resep_durasi: "", troli_satu: "", troli_dua: "", troli_tiga: "", std_time_min: "", std_time_max: "" }, // passing the values
      success: function (res) {
         document.getElementById("a-home").click();
         $("#modal-edit-resep").modal("hide");
         $("#modal-pr-pyn").modal("hide");
      },
      fail: function (onfail) {
         document.getElementById("a-home").click();
         $("#modal-edit-resep").modal("hide");
      },
   });
}
