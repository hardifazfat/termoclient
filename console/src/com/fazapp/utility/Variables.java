package com.fazapp.utility;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public enum Variables {
    GET_CURRENT_PV, GET_CURRENT_SV,
    ERROR, GET_CURRENT_PVSV, WRITE_SV, FORCE_STOP, GET_COIL_STATUS,
    ENABLE_ALARM, DISABLE_ALARM, SET_RUN, CLEAR_SV;
}
