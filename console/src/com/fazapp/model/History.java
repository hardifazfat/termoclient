package com.fazapp.model;

public class History {

    private int id;
    private String task_id, remark, heating_timestamp, cooking_timestamp, finish_timestamp;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setCooking_timestamp(String cooking_timestamp) {
        this.cooking_timestamp = cooking_timestamp;
    }

    public String getCooking_timestamp() {
        return cooking_timestamp;
    }

    public void setFinish_timestamp(String finish_timestamp) {
        this.finish_timestamp = finish_timestamp;
    }

    public String getFinish_timestamp() {
        return finish_timestamp;
    }

    public void setHeating_timestamp(String heating_timestamp) {
        this.heating_timestamp = heating_timestamp;
    }

    public String getHeating_timestamp() {
        return heating_timestamp;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }
}
