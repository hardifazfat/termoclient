package com.fazapp.model;

import com.fazapp.interf.Values;
import com.fazapp.utility.CRC16Modbus;
import com.fazapp.utility.Variables;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class Mesin {
    private String c_read_pvsv = "0403E80004";
    private String c_writesv = "060039005C";
    private String c_clearsv = "0600390000";
    private String c_force_on = "0500000000";
    private String c_force_stop = "050000FF00";
    private String c_read_coil = "0403EE0001";
    private String c_set_sv_0 = "1000000001020000";
    private String c_set_alarm_on = "0600360004";
    private String c_set_alarm_off = "06003600FF";
    private String ret_val;
    private SerialPort serialPort;
    private mesinListener listener;
    private Variables mode;
    private boolean isBusy;

    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);
    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }

    private SerialPortEventListener eventListener = new SerialPortEventListener() {
        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {
            if(serialPortEvent.isRXCHAR()){
                String buffs = null;
                if(mode == Variables.GET_CURRENT_PVSV){
                    if(serialPortEvent.getEventValue() == 13){
                        try {
                            byte[] bt = serialPort.readBytes(13);
                            buffs = bytesToHex(bt);

                            ret_val = (String)calculate(buffs);

                            String id = ret_val.split(Values.GENERAL_SEPARATOR)[0];
                            readCoil(id);
                            //listener.onOperationResult(Variables.GET_CURRENT_PVSV, calculate(buffs));
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                            System.out.println("get pv len :" + e.getMessage());
                        }
                    }
                    else if(serialPortEvent.getEventValue() == 5){
                        isBusy = false;
                        try {
                            byte[] bt = serialPort.readBytes(serialPortEvent.getEventValue());
                            //listener.onOperationResult(Variables.GET_CURRENT_PVSV, calculate(buffs));
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(serialPortEvent.getEventValue() > 13){
                        try {
                            byte[] bt = serialPort.readBytes(serialPortEvent.getEventValue());

                            isBusy = false;
                            //listener.onOperationResult(Variables.GET_CURRENT_PVSV, calculate(buffs));
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(mode == Variables.GET_COIL_STATUS){
                    if(serialPortEvent.getEventValue() == 7){
                        try {
                            isBusy = false;
                            byte[] bt = serialPort.readBytes(7);
                            buffs = bytesToHex(bt);

                            ret_val = calculate(buffs) + ret_val;

                            listener.onOperationResult(Variables.GET_CURRENT_PVSV, ret_val);
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(serialPortEvent.getEventValue() == 5){
                        try {
                            byte[] bt = serialPort.readBytes(5);
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                        isBusy = false;
                    }
                }
                else if(mode == Variables.WRITE_SV){
                    if(serialPortEvent.getEventValue() == 8){
                        try {
                            byte[] bt = serialPort.readBytes(8);
                            enableAlarm("01");
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(mode == Variables.ENABLE_ALARM){
                    if(serialPortEvent.getEventValue() == 8){
                        try {
                            byte[] bt = serialPort.readBytes(8);
                            forceON("01");
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(mode == Variables.SET_RUN){
                    if(serialPortEvent.getEventValue() == 8){
                        try {
                            byte[] bt = serialPort.readBytes(8);
                            isBusy = false;
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(mode == Variables.FORCE_STOP){
                    if(serialPortEvent.getEventValue() == 8){
                        try {
                            byte[] bt = serialPort.readBytes(8);
                            disableAlarm("01");
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(mode == Variables.DISABLE_ALARM){
                    if(serialPortEvent.getEventValue() == 8){
                        try {
                            byte[] bt = serialPort.readBytes(8);
                            clearSV("01");
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(mode == Variables.CLEAR_SV){
                    if(serialPortEvent.getEventValue() == 8){
                        try {
                            byte[] bt = serialPort.readBytes(8);
                            isBusy = false;
                        } catch (SerialPortException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }
    };

    public interface mesinListener{
        void onOperationResult(Variables code, Object val);
    }

    public void setSerialPort(SerialPort serialPort){
        this.serialPort = serialPort;

        try {
            this.serialPort.addEventListener(eventListener);
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    public void setListener(mesinListener listener) {
        this.listener = listener;
    }

    public void readCoil(String id){
        this.mode = Variables.GET_COIL_STATUS;
        String command = id + c_read_coil;

        commandProtocol(command);
    }
    public void forceSTOP(String id){
        this.mode = Variables.FORCE_STOP;
        String command = id + c_force_stop;

        //if(!isBusy){
            commandProtocol(command);
            isBusy = true;
       // }

    }

    public void forceON(String id){
        this.mode = Variables.SET_RUN;
        String command = id + c_force_on;

        commandProtocol(command);
    }

    private void enableAlarm(String id){
        this.mode = Variables.ENABLE_ALARM;

        String command = "01" + c_set_alarm_on;

        //if(!isBusy)
        commandProtocol(command);
    }

    private void disableAlarm(String id){
        this.mode = Variables.DISABLE_ALARM;
        String command = "01" + c_set_alarm_off;

        //if(!isBusy)
        commandProtocol(command);
    }

    public void setSV(String id){
        this.mode = Variables.WRITE_SV;
        String command = "01" + c_writesv;

      //  if(!isBusy) {
            commandProtocol(command);
            isBusy = true;
       // }
    }

    public void clearSV(String id){
        this.mode = Variables.CLEAR_SV;
        String command = "01" + c_clearsv;

        commandProtocol(command);
    }

    public void getCurrentPV(String id){
        this.mode = Variables.GET_CURRENT_PVSV;
        String command = id + c_read_pvsv;

       // if(!isBusy){
            commandProtocol(command);
            isBusy = true;
       // }
    }

    private void commandProtocol(String cmd){
        byte[] buffer = DatatypeConverter.parseHexBinary(cmd);

        CRC16Modbus crc_ = new CRC16Modbus();
        int[] data = new int[buffer.length];
        byte[] data_byte = new byte[buffer.length + 2];

        for(int i = 0;i < buffer.length;i++){
            data_byte[i] = buffer[i];
            data[i] = Byte.toUnsignedInt(buffer[i]);
        }

        for (int d : data) {
            crc_.update(d);
        }

        data_byte[buffer.length] = (byte) ((crc_.getValue() & 0x000000ff));
        data_byte[buffer.length + 1] = (byte) ((crc_.getValue() & 0x0000ff00) >>> 8);

        try {
            serialPort.writeBytes(data_byte);
        } catch (SerialPortException e) {
            e.printStackTrace();
            listener.onOperationResult(Variables.ERROR, 0);
        }
    }

    private Object calculate(String hexString){
        int ret_val = 0;
        String ret_str = "";

        if(mode == Variables.GET_CURRENT_PVSV){
            String address = hexString.substring(0,2);
            String pv_str = hexString.substring(6,10);
            String sv_str = hexString.substring(18,22);

            int pv = Integer.parseInt(pv_str,16);
            int sv = Integer.parseInt(sv_str, 16);

            return String.format("%s%s%d%s%d",address,Values.GENERAL_SEPARATOR,pv, Values.GENERAL_SEPARATOR, sv);
        }
        else if(mode == Variables.GET_COIL_STATUS){
            String coil = hexString.substring(8,10);
            String coil_alarm = hexString.substring(6,8);

            int coil_a_int = Integer.parseInt(coil_alarm) << 2;

            coil = Integer.parseInt(coil) > 7 ? "1":"0";
            coil_alarm = coil_a_int > 7 ? "1":"0";

            return String.format("%s%s%s%s",coil,Values.GENERAL_SEPARATOR,coil_alarm,Values.GENERAL_SEPARATOR);
        }
        return null;
    }
}
