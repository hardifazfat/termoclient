package com.fazapp.model;

import com.google.gson.annotations.SerializedName;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class Task {

    private int inc, mesin_id, resep_id;
    private String items, colors, lots, pic, additional, id;

    public void setInc(int inc) {
        this.inc = inc;
    }

    public int getInc() {
        return inc;
    }

    public int getMesin_id() {
        return mesin_id;
    }

    public void setMesin_id(int mesin_id) {
        this.mesin_id = mesin_id;
    }

    public int getResep_id() {
        return resep_id;
    }

    public void setResep_id(int resep_id) {
        this.resep_id = resep_id;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getLots() {
        return lots;
    }

    public void setLots(String lots) {
        this.lots = lots;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
