package com.fazapp.model;

public class ActiveTask {
    private String task_id;
    private int inc;

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setInc(int inc) {
        this.inc = inc;
    }

    public int getInc() {
        return inc;
    }
}
