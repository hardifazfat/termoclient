package com.fazapp.model;

import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class CountDownTimer {
    private long timeFinish, timeTick;
    private cd_listener listener;
    private Timer timer;
    private ScheduledExecutorService scheduler;
    private Runnable tmr_runnable;

    public CountDownTimer(long timeFinish, long timeTick){
        this.timeFinish = timeFinish;
        this.timeTick = timeTick;

    }

    public void startTimer(){
        tmr_runnable = new Runnable() {
            long cd_value = timeFinish;
            long increment = 0;
            public void run() {
                increment++;
                if(increment == timeTick){
                    increment = 0;
                    cd_value -= timeTick;
                    listener.onTick(cd_value);
                }

                if (cd_value < 1) {
                    scheduler.shutdown();
                    listener.onFinish();
                }
            }
        };
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.scheduler.scheduleAtFixedRate(tmr_runnable, 0, 1, TimeUnit.MILLISECONDS);
    }

    public interface cd_listener{
        void onTick(long ms_remaining);
        void onFinish();
    }

    public void setListener(cd_listener listener){
        this.listener = listener;
    }


}
