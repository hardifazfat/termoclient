package com.fazapp;

import com.fazapp.API.APIClient;
import com.fazapp.APIModel.GETActiveTask;
import com.fazapp.APIModel.GETTask;
import com.fazapp.interf.APIInterface;
import com.fazapp.interf.Values;
import com.fazapp.model.*;
import com.fazapp.utility.Variables;
import javafx.scene.paint.Stop;
import jssc.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import static jssc.SerialPort.*;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class Main {
    private static String current_task_id;
    private static CountDownTimer task_timer;
    private static APIInterface client_interace;
    private static Mesin mesin;
    private static boolean isReadDone = true, request_write = false;
    private static int index = 0;

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    private static SimpleDateFormat sdf_backend_format = new SimpleDateFormat("yyyyMMddhhmmss");

    public static void main(String[] args) {
        mesin = new Mesin();
        client_interace = APIClient.getClient().create(APIInterface.class);

        String com_name = "";

        try {
            File myObj = new File("config.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                com_name = data;
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        //SerialPort serialPort = new SerialPort("COM7");
        SerialPort serialPort = new SerialPort(com_name);
        if(!serialPort.isOpened()){
            try {
                serialPort.openPort();
                serialPort.setParams(BAUDRATE_38400,  DATABITS_8, STOPBITS_2, PARITY_NONE);

                mesin.setSerialPort(serialPort);
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }

        mesin.setListener(new Mesin.mesinListener() {
            @Override
            public void onOperationResult(Variables code, Object val) {
                if(code == Variables.GET_CURRENT_PVSV){
                    String[] val_str = ((String)val).split(Values.GENERAL_SEPARATOR);
                    int coil_status = Integer.parseInt(val_str[0]);
                    int coil_alarm_status = Integer.parseInt(val_str[1]);
                    String address = val_str[2];
                    int pv = Integer.parseInt(val_str[3]);
                    int sv = Integer.parseInt(val_str[4]);

                    if(sv != 92){
                        mesin.setSV(address);
                    }

                    Call<APIResponse> GAT = client_interace.postUpdate(2, current_task_id, pv, sv, coil_status,coil_alarm_status);
                    GAT.enqueue(new Callback<APIResponse>() {
                        @Override
                        public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {

                        }

                        @Override
                        public void onFailure(Call<APIResponse> call, Throwable throwable) {

                        }
                    });

                    String msg = String.format("Current PV = %d, SV = %d, Coil : %d, Coil alarm : %d",
                            pv, sv, coil_status,coil_alarm_status);
                    System.out.println(msg);
                }
            }
        });

        taskPinger();

    }

    private static void taskPinger(){
        task_timer = new CountDownTimer(1000, 1000);
        task_timer.setListener(new CountDownTimer.cd_listener() {
            @Override
            public void onTick(long ms_remaining) {
                String msg = String.format("New task pinger on %s", sdf.format(System.currentTimeMillis()));
                System.out.println(msg);

                Call<GETTask> getTaskCall = client_interace.get_task();
                Call<GETActiveTask> getActiveTaskCall = client_interace.getActiveTask(4);
                getActiveTaskCall.enqueue(new Callback<GETActiveTask>() {
                    @Override
                    public void onResponse(Call<GETActiveTask> call, Response<GETActiveTask> response) {
                        GETActiveTask trs = response.body();
                        List<ActiveTask> list = trs.getListActiveTask();
                        HashMap<Integer, StopTask> map_stop = trs.getMapStop();

                        for(StopTask task:map_stop.values()){
                            String task_id = task.getTask_id();
                            Task tasks = trs.getMapTask().get(task_id);
                            Steam steam = (null == tasks)? null:trs.getMapSteam().get(tasks.getMesin_id());

                            System.out.println("Force stop on task id: " + task_id);

                            if(null != steam){
                                request_write = true;
                                if(isReadDone){
                                    request_write = false;
                                    mesin.forceSTOP(steam.getAddress());
                                    Call<APIResponse> DF = client_interace.deleteFinish(12, task_id);
                                    DF.enqueue(new Callback<APIResponse>() {
                                        @Override
                                        public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {

                                        }

                                        @Override
                                        public void onFailure(Call<APIResponse> call, Throwable throwable) {

                                        }
                                    });
                                }
                            }
                        }

                        for(ActiveTask at:list){
                            String task_id = at.getTask_id();
                            Task task = trs.getMapTask().get(task_id);
                            Steam steam = trs.getMapSteam().get(task.getMesin_id());
                            Resep resep = trs.getMapResep().get(task.getResep_id());
                            History history = trs.getMapHistory().get(task_id);

                            String cooking_time = history.getCooking_timestamp();
                            if(!cooking_time.equalsIgnoreCase("0")){
                                Date date = null;
                                try {
                                    date = sdf_backend_format.parse(cooking_time);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long cooking_time_milis = date.getTime();

                                String[] additional_time_split = task.getAdditional().split(",,,");
                                long durasi = resep.getDurasi() * 60000;

                                for(String str : additional_time_split){
                                    long add_time = Long.parseLong(str) * 60000;

                                    durasi += add_time;
                                }

                                String msg = String.format("Task id : %s finish on : %s (durasi : %d min)", task_id, sdf.format(cooking_time_milis + durasi), durasi / 60000 );
                                System.out.println(msg);

                                if((System.currentTimeMillis() - cooking_time_milis) > durasi){
                                    request_write = true;
                                    if(isReadDone){
                                        Call<APIResponse> PF = client_interace.postFinish(11, task_id);
                                        PF.enqueue(new Callback<APIResponse>() {
                                            @Override
                                            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                                                System.out.println("posting task finish id :" + task_id);
                                                mesin.forceSTOP(steam.getAddress());

                                                request_write = false;
                                            }

                                            @Override
                                            public void onFailure(Call<APIResponse> call, Throwable throwable) {

                                            }
                                        });
                                    }
                                }
                            }
                        }

                        if(isReadDone && !request_write){
                            isReadDone = false;
                            startMonitoringPV(trs);
                        }
                    }

                    @Override
                    public void onFailure(Call<GETActiveTask> call, Throwable throwable) {
                        System.out.println(111);
                    }
                });
                //getTaskCall.enqueue(getTaskCallback);
            }

            @Override
            public void onFinish() {
                task_timer.startTimer();
            }
        });

        task_timer.startTimer();
    }

    private static void startMonitoringPV(GETActiveTask get){
        List<ActiveTask> list = get.getListActiveTask();
        int count = 200 * list.size();

        index = 0;

        CountDownTimer cd = new CountDownTimer(count, 200);
        cd.setListener(new CountDownTimer.cd_listener() {
            @Override
            public void onTick(long ms_remaining) {

                ActiveTask activeTask = list.get(index);
                current_task_id = activeTask.getTask_id();

                Task task = get.getMapTask().get(current_task_id);
                Steam steam = get.getMapSteam().get(task.getMesin_id());

                mesin.getCurrentPV(steam.getAddress());

                index += 1;
                //if(ms_remaining == 0)
                    //taskPinger();
            }

            @Override
            public void onFinish() {
                isReadDone = true;
            }
        });
        cd.startTimer();
    }
}
