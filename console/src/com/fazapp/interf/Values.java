package com.fazapp.interf;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public interface Values {
    String GENERAL_SEPARATOR = "T3RM0C0N";
    int START_MONITORING_PV = 0;
    int STOP_MONITORING_PV = 1;
}
