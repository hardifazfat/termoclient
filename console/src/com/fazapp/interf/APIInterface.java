package com.fazapp.interf;

import com.fazapp.APIModel.GETActiveTask;
import com.fazapp.APIModel.GETTask;
import com.fazapp.APIModel.Mesin;
import com.fazapp.model.APIResponse;
import com.fazapp.model.ActiveTask;
import retrofit2.Call;
import retrofit2.http.*;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public interface APIInterface {
    @GET("mesin")
    Call<Mesin> call_mesin();
    @GET("postdm.php")
    Call<GETActiveTask> getActiveTask(@Query("action_id") int id);

    @FormUrlEncoded
    @POST("postdm.php")
    Call<APIResponse> postUpdate(@Field("action_id") int id,
                           @Field("task_id") String task_id,
                           @Field("pv") int pv,
                           @Field("sv") int sv,
                                 @Field("is_latch") int is_latch,
                                 @Field("is_latch_2") int is_latch_2);

    @FormUrlEncoded
    @POST("postdm.php")
    Call<APIResponse> deleteFinish(@Field("action_id") int id,
                                 @Field("task_id") String task_id);

    @FormUrlEncoded
    @POST("postdm.php")
    Call<APIResponse> postFinish(@Field("action_id") int id,
                                 @Field("task_id") String task_id);

    @FormUrlEncoded
    @POST("postdm.php")
    Call<APIResponse> deleteTask(@Field("id") int id, @Field("action_id") int action_id);

    @GET("gettask.php")
    Call<GETTask> get_task();
}
