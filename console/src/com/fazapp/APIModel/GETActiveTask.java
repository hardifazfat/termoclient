package com.fazapp.APIModel;

import com.fazapp.model.*;
import com.google.gson.annotations.SerializedName;
import javafx.scene.paint.Stop;

import java.util.HashMap;
import java.util.List;

public class GETActiveTask {
    @SerializedName("value_active_task")
    List<ActiveTask> listActiveTask;
    @SerializedName("value_task")
    List<Task> listTask;
    @SerializedName("value_history")
    List<History> listHistory;
    @SerializedName("value_resep")
    List<Resep> listResep;
    @SerializedName("value_steam")
    List<Steam> listSteam;
    @SerializedName("value_force")
    List<StopTask> listStop;

    public List<ActiveTask> getListActiveTask() {
        return listActiveTask;
    }

    public HashMap<String, Task> getMapTask(){
        HashMap<String, Task> map = new HashMap<>();
        for(Task task:listTask){
            map.put(task.getId(), task);
        }
        return map;
    }
    public HashMap<Integer, StopTask> getMapStop(){
        HashMap<Integer, StopTask> map = new HashMap<>();

        for(StopTask task:listStop){
            map.put(task.getId(), task);
        }

        return map;
    }

    public HashMap<Integer, Steam> getMapSteam(){
        HashMap<Integer, Steam> map = new HashMap<>();

        for(Steam steam:listSteam){
            map.put(steam.getId(), steam);
        }

        return map;
    }

    public HashMap<String, History> getMapHistory(){
        HashMap<String, History> map = new HashMap<>();

        for(History history:listHistory){
            map.put(history.getTask_id(), history);
        }

        return map;
    }

    public HashMap<Integer, Resep> getMapResep(){
        HashMap<Integer, Resep> map = new HashMap<>();

        for(Resep resep:listResep){
            map.put(resep.getId(), resep);
        }

        return map;
    }

    public List<Task> getListTask() {
        return listTask;
    }

    public List<History> getListHistory() {
        return listHistory;
    }

    public List<Steam> getListSteam() {
        return listSteam;
    }
}
