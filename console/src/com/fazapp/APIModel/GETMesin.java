package com.fazapp.APIModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class GETMesin {
    @SerializedName("result")
    List<Mesin> listDataMesin;
}
