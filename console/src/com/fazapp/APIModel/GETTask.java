package com.fazapp.APIModel;

import com.fazapp.model.Task;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class GETTask {
    @SerializedName("value")
    List<Task> listTask;

    public List<Task> getListTask() {
        return listTask;
    }

    public void setListTask(List<Task> listTask) {
        this.listTask = listTask;
    }
}
