package com.fazapp.APIModel;

import com.google.gson.annotations.SerializedName;

/*
Created by : Hardianto DwiPA @FAZ.App
Jan, 2022
 */

public class Mesin {
    @SerializedName("label")
    String label;
    @SerializedName("id")
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
