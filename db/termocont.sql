-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2022 at 05:27 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `termocont`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_task`
--

CREATE TABLE `active_task` (
  `inc` int(11) NOT NULL,
  `task_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` int(11) NOT NULL,
  `nama` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `nama`) VALUES
(1, 'Merah');

-- --------------------------------------------------------

--
-- Table structure for table `culikan_result`
--

CREATE TABLE `culikan_result` (
  `id` int(11) NOT NULL,
  `resep_id` int(11) NOT NULL DEFAULT 0,
  `validate` varchar(20) NOT NULL,
  `tambahan` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `culikan_result`
--

INSERT INTO `culikan_result` (`id`, `resep_id`, `validate`, `tambahan`) VALUES
(1, 19, '20220318011615', 0),
(2, 20, '20220412111407', 0),
(3, 20, '20220412124539', 0),
(4, 20, '20220412125557', 0),
(5, 20, '20220412170020', 0),
(6, 20, '20220412172555', 0),
(7, 20, '20220412172736', 0),
(8, 20, '20220412172858', 0),
(9, 20, '20220412181936', 0),
(10, 20, '20220412214024', -1),
(11, 20, '20220412215721', -1),
(12, 20, '20220412215721', -10),
(13, 20, '20220412220052', -10),
(14, 20, '20220412220353', -10),
(15, 20, '20220412220353', -1),
(16, 20, '20220412220353', -11),
(17, 20, '20220412220609', -1),
(18, 20, '20220412220628', -1),
(19, 20, '20220412220628', -10),
(20, 20, '20220412220628', -11),
(21, 20, '20220412221322', -11),
(22, 20, '20220412221458', -11),
(23, 20, '20220412222331', -11);

-- --------------------------------------------------------

--
-- Table structure for table `force_stop`
--

CREATE TABLE `force_stop` (
  `id` int(11) NOT NULL,
  `task_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `task_id` varchar(15) NOT NULL,
  `heating_timestamp` bigint(20) NOT NULL,
  `cooking_timestamp` bigint(20) NOT NULL,
  `finish_timestamp` bigint(20) NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mesin`
--

CREATE TABLE `mesin` (
  `id` int(11) NOT NULL,
  `label` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mesin`
--

INSERT INTO `mesin` (`id`, `label`) VALUES
(1, 'MESIN-1'),
(2, 'mesin3'),
(3, 'mesin4'),
(4, 'mesin55'),
(5, 'mesin55'),
(6, 'mesin55'),
(7, 'mesin55'),
(8, 'mesin55'),
(9, 'mesin55');

-- --------------------------------------------------------

--
-- Table structure for table `recent`
--

CREATE TABLE `recent` (
  `id` int(11) NOT NULL,
  `task_id` varchar(20) NOT NULL,
  `pv` int(11) NOT NULL,
  `sv` int(11) NOT NULL,
  `is_latch` int(11) NOT NULL,
  `ts` varchar(20) NOT NULL,
  `is_latch_2` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `recent_condition`
--

CREATE TABLE `recent_condition` (
  `mesin_id` varchar(2) NOT NULL,
  `last_input` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `pv` int(11) NOT NULL,
  `sv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recent_condition`
--

INSERT INTO `recent_condition` (`mesin_id`, `last_input`, `pv`, `sv`) VALUES
('01', '2022-01-16 12:00:54', 41, 24);

-- --------------------------------------------------------

--
-- Table structure for table `resep`
--

CREATE TABLE `resep` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `durasi` int(11) NOT NULL,
  `troli_satu` int(5) NOT NULL DEFAULT 0,
  `troli_dua` int(5) NOT NULL DEFAULT 0,
  `troli_tiga` int(5) NOT NULL DEFAULT 0,
  `std_cook_min` int(5) NOT NULL DEFAULT 0,
  `std_cook_max` int(5) NOT NULL DEFAULT 0,
  `per_batch` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `setting_variables`
--

CREATE TABLE `setting_variables` (
  `id` int(11) NOT NULL,
  `heating_std` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `setting_variables`
--

INSERT INTO `setting_variables` (`id`, `heating_std`) VALUES
(1, 25);

-- --------------------------------------------------------

--
-- Table structure for table `steam`
--

CREATE TABLE `steam` (
  `id` int(11) NOT NULL,
  `label` varchar(20) NOT NULL,
  `address` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `steam`
--

INSERT INTO `steam` (`id`, `label`, `address`) VALUES
(1, '1', '01');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `inc` int(11) NOT NULL,
  `mesin_id` varchar(2) NOT NULL,
  `resep_id` int(11) NOT NULL,
  `items` text DEFAULT NULL,
  `colors` text DEFAULT NULL,
  `lots` varchar(50) DEFAULT NULL,
  `pic` varchar(50) NOT NULL,
  `additional` varchar(80) NOT NULL,
  `id` varchar(15) NOT NULL,
  `troli` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `role` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
(1, 'superadmin', 'superadmin', 'Super Admin'),
(2, 'admin', 'admin', 'Admin'),
(3, 'operator', 'operator', 'Operator'),
(4, 'abdul', 'abcd', 'Operator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_task`
--
ALTER TABLE `active_task`
  ADD PRIMARY KEY (`inc`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `culikan_result`
--
ALTER TABLE `culikan_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `force_stop`
--
ALTER TABLE `force_stop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recent`
--
ALTER TABLE `recent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_variables`
--
ALTER TABLE `setting_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `steam`
--
ALTER TABLE `steam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`inc`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active_task`
--
ALTER TABLE `active_task`
  MODIFY `inc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `culikan_result`
--
ALTER TABLE `culikan_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `force_stop`
--
ALTER TABLE `force_stop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `mesin`
--
ALTER TABLE `mesin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `recent`
--
ALTER TABLE `recent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `resep`
--
ALTER TABLE `resep`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `setting_variables`
--
ALTER TABLE `setting_variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `steam`
--
ALTER TABLE `steam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `inc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
